--
-- PostgreSQL database dump
--

-- Dumped from database version 12.3 (Debian 12.3-1.pgdg100+1)
-- Dumped by pg_dump version 12rc1

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: customers; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE public.customers (
    id integer NOT NULL,
    address character varying NOT NULL,
    name character varying NOT NULL,
    gender integer NOT NULL,
    birth_date date NOT NULL,
    phone_number character varying NOT NULL,
    extra jsonb,
    meta jsonb,
    "createdAt" timestamp with time zone,
    "updatedAt" timestamp with time zone,
    "deletedAt" timestamp with time zone,
    city_id integer NOT NULL,
    code character varying,
    district_id integer NOT NULL
);


ALTER TABLE public.customers OWNER TO root;

--
-- Name: customers_id_seq; Type: SEQUENCE; Schema: public; Owner: root
--

CREATE SEQUENCE public.customers_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.customers_id_seq OWNER TO root;

--
-- Name: customers_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: root
--

ALTER SEQUENCE public.customers_id_seq OWNED BY public.customers.id;


--
-- Name: employees; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE public.employees (
    id integer NOT NULL,
    email character varying NOT NULL,
    password character varying NOT NULL,
    name character varying NOT NULL,
    city_id integer NOT NULL,
    district_id integer NOT NULL,
    address character varying NOT NULL,
    gender integer NOT NULL,
    birth_date date NOT NULL,
    phone_number character varying NOT NULL,
    extra jsonb,
    meta jsonb,
    "createdAt" timestamp without time zone,
    "updatedAt" timestamp without time zone,
    "deletedAt" timestamp without time zone,
    role_id integer NOT NULL
);


ALTER TABLE public.employees OWNER TO root;

--
-- Name: employees_id_seq; Type: SEQUENCE; Schema: public; Owner: root
--

CREATE SEQUENCE public.employees_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.employees_id_seq OWNER TO root;

--
-- Name: employees_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: root
--

ALTER SEQUENCE public.employees_id_seq OWNED BY public.employees.id;


--
-- Name: roles; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE public.roles (
    id integer NOT NULL,
    name character varying NOT NULL,
    "createdAt" timestamp with time zone,
    "updatedAt" timestamp with time zone,
    "deletedAt" timestamp with time zone
);


ALTER TABLE public.roles OWNER TO root;

--
-- Name: employees_role_id_seq; Type: SEQUENCE; Schema: public; Owner: root
--

CREATE SEQUENCE public.employees_role_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.employees_role_id_seq OWNER TO root;

--
-- Name: employees_role_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: root
--

ALTER SEQUENCE public.employees_role_id_seq OWNED BY public.roles.id;


--
-- Name: locations; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE public.locations (
    id integer NOT NULL,
    name character varying NOT NULL,
    "createdAt" timestamp with time zone,
    "updatedAt" timestamp with time zone,
    "deletedAt" timestamp with time zone,
    type integer,
    parent_id integer
);


ALTER TABLE public.locations OWNER TO root;

--
-- Name: province_id_seq; Type: SEQUENCE; Schema: public; Owner: root
--

CREATE SEQUENCE public.province_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.province_id_seq OWNER TO root;

--
-- Name: province_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: root
--

ALTER SEQUENCE public.province_id_seq OWNED BY public.locations.id;


--
-- Name: rank; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE public.rank (
    id integer NOT NULL,
    name character varying NOT NULL,
    type integer,
    "parent_Id" integer,
    "createdAt" timestamp with time zone,
    "updateAt" timestamp with time zone,
    "deletedAt" timestamp with time zone
);


ALTER TABLE public.rank OWNER TO root;

--
-- Name: rank_id_seq; Type: SEQUENCE; Schema: public; Owner: root
--

CREATE SEQUENCE public.rank_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.rank_id_seq OWNER TO root;

--
-- Name: rank_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: root
--

ALTER SEQUENCE public.rank_id_seq OWNED BY public.rank.id;


--
-- Name: rental_transaction; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE public.rental_transaction (
    customer_id integer NOT NULL,
    employee_id integer NOT NULL,
    name_hotel integer NOT NULL,
    name_customer integer NOT NULL,
    time_rental timestamp with time zone NOT NULL,
    time_over_rental timestamp with time zone NOT NULL,
    room_id integer NOT NULL
);


ALTER TABLE public.rental_transaction OWNER TO root;

--
-- Name: rooms; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE public.rooms (
    id integer NOT NULL,
    start_time timestamp with time zone,
    end_time timestamp with time zone,
    status integer NOT NULL,
    rank_title character varying NOT NULL,
    meta jsonb NOT NULL,
    extra jsonb NOT NULL,
    rank_type_title character varying NOT NULL,
    price_rental double precision NOT NULL,
    "createdAt" timestamp with time zone,
    "updatedAt" timestamp with time zone,
    "deletedAt" timestamp with time zone,
    code character varying NOT NULL
);


ALTER TABLE public.rooms OWNER TO root;

--
-- Name: rooms_id_seq; Type: SEQUENCE; Schema: public; Owner: root
--

CREATE SEQUENCE public.rooms_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.rooms_id_seq OWNER TO root;

--
-- Name: rooms_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: root
--

ALTER SEQUENCE public.rooms_id_seq OWNED BY public.rooms.id;


--
-- Name: customers id; Type: DEFAULT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.customers ALTER COLUMN id SET DEFAULT nextval('public.customers_id_seq'::regclass);


--
-- Name: employees id; Type: DEFAULT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.employees ALTER COLUMN id SET DEFAULT nextval('public.employees_id_seq'::regclass);


--
-- Name: locations id; Type: DEFAULT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.locations ALTER COLUMN id SET DEFAULT nextval('public.province_id_seq'::regclass);


--
-- Name: rank id; Type: DEFAULT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.rank ALTER COLUMN id SET DEFAULT nextval('public.rank_id_seq'::regclass);


--
-- Name: roles id; Type: DEFAULT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.roles ALTER COLUMN id SET DEFAULT nextval('public.employees_role_id_seq'::regclass);


--
-- Name: rooms id; Type: DEFAULT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.rooms ALTER COLUMN id SET DEFAULT nextval('public.rooms_id_seq'::regclass);


--
-- Data for Name: customers; Type: TABLE DATA; Schema: public; Owner: root
--

INSERT INTO public.customers VALUES
	(60, '111 ha noi', 'Thi Thi', 1, '1999-11-02', '0908780370', NULL, NULL, '2020-08-19 03:10:11.836+00', '2020-08-19 03:10:11.836+00', NULL, 2, '4221', 78),
	(19, '111 ha noi', 'Thi Thi', 2, '1999-11-02', '0908780370', NULL, NULL, '2020-08-11 08:15:17.711+00', '2020-08-11 08:15:17.711+00', NULL, 1, '310', 65),
	(20, '111 ha noi', 'Thi Thi', 1, '1999-11-02', '0908780370', NULL, NULL, '2020-08-11 08:19:31.526+00', '2020-08-11 08:19:31.526+00', NULL, 1, '311', 65),
	(21, '111 ha noi', 'Thi Thi', 2, '1999-11-02', '0908780370', NULL, NULL, '2020-08-11 08:19:45.008+00', '2020-08-11 08:19:45.008+00', NULL, 1, '312', 64),
	(23, '111 ha noi', 'Thi Thi', 2, '1999-11-02', '0908780370', NULL, NULL, '2020-08-11 09:12:59.358+00', '2020-08-11 09:12:59.358+00', NULL, 2, '313', 78),
	(26, '111 ha noi', 'Thi Thi', 1, '1999-11-02', '0908780370', NULL, NULL, '2020-08-14 07:19:58.278+00', '2020-08-14 07:19:58.278+00', NULL, 2, '3133', 78),
	(62, '111 ha noi', 'Thi Thi', 1, '1999-11-02', '0908780370', NULL, NULL, '2020-08-19 03:18:20.369+00', '2020-08-19 03:18:20.369+00', NULL, 2, '42213', 78),
	(28, '111 ha noi', 'Thi Thi', 1, '1999-11-02', '0908780370', NULL, NULL, '2020-08-14 08:03:42.261+00', '2020-08-14 08:03:42.261+00', NULL, 2, '31233', 78),
	(33, '111 ha noi', 'Thi Thi', 1, '1999-11-02', '0908780370', NULL, NULL, '2020-08-17 09:17:16.126+00', '2020-08-17 09:17:16.126+00', NULL, 2, '312233', 78),
	(34, '111 ha noi', 'Thi Thi', 1, '1999-11-02', '0908780370', NULL, NULL, '2020-08-17 09:17:51.043+00', '2020-08-17 09:17:51.043+00', NULL, 2, '3122233', 78),
	(35, '111 ha noi', 'Thi Thi', 1, '1999-11-02', '0908780370', NULL, NULL, '2020-08-17 09:55:44.426+00', '2020-08-17 09:55:44.426+00', NULL, 2, '31232233', 78),
	(37, '111 ha noi', 'Thi Thi', 1, '1999-11-02', '0908780370', NULL, NULL, '2020-08-18 07:25:24.156+00', '2020-08-18 07:25:24.156+00', NULL, 2, '312132233', 78),
	(38, '111 ha noi', 'Thi Thi', 1, '1999-11-02', '0908780370', NULL, NULL, '2020-08-18 07:28:57.732+00', '2020-08-18 07:28:57.732+00', NULL, 2, '3121322333', 78),
	(10, '111 ha noi', 'Thi Thi', 1, '1999-11-02', '0908780370', NULL, NULL, '2020-08-11 07:36:49.019+00', '2020-08-11 07:36:49.019+00', NULL, 1, '302', 65),
	(11, '111 ha noi', 'Thi Thi', 2, '1999-11-02', '0908780370', NULL, NULL, '2020-08-11 07:45:58.422+00', '2020-08-11 07:45:58.422+00', NULL, 1, '303', 65),
	(13, '111 ha noi', 'Thi Thi', 1, '1999-11-02', '0908780370', NULL, NULL, '2020-08-11 07:51:43.645+00', '2020-08-11 07:51:43.645+00', NULL, 1, '304', 65),
	(14, '111 ha noi', 'Thi Thi', 2, '1999-11-02', '0908780370', NULL, NULL, '2020-08-11 08:00:43.518+00', '2020-08-11 08:00:43.518+00', NULL, 1, '305', 65),
	(40, '111 ha noi', 'Thi Thi', 1, '1999-11-02', '0908780370', NULL, NULL, '2020-08-18 07:33:12.758+00', '2020-08-18 07:33:12.758+00', NULL, 2, '13121322333', 78),
	(15, '111 ha noi', 'Thi Thi', 2, '1999-11-02', '0908780370', NULL, NULL, '2020-08-11 08:09:49.451+00', '2020-08-11 08:09:49.451+00', NULL, 1, '307', 65),
	(16, '111 ha noi', 'Thi Thi', 1, '1999-11-02', '0908780370', NULL, NULL, '2020-08-11 08:10:10.025+00', '2020-08-11 08:10:10.025+00', NULL, 1, '308', 65),
	(17, '111 ha noi', 'Thi Thi', 2, '1999-11-02', '0908780370', NULL, NULL, '2020-08-11 08:13:42.125+00', '2020-08-11 08:13:42.125+00', NULL, 1, '309', 65),
	(42, '111 ha noi', 'Thi Thi', 1, '1999-11-02', '0908780370', NULL, NULL, '2020-08-18 07:38:45.314+00', '2020-08-18 07:38:45.314+00', NULL, 2, '131213322333', 78),
	(43, '111 ha noi', 'Thi Thi', 1, '1999-11-02', '0908780370', NULL, NULL, '2020-08-18 08:10:50.072+00', '2020-08-18 08:10:50.072+00', NULL, 2, '1312133322333', 78),
	(44, '111 ha noi', 'Thi Thi', 1, '1999-11-02', '0908780370', NULL, NULL, '2020-08-18 08:16:53.188+00', '2020-08-18 08:16:53.188+00', NULL, 2, '1031233', 78),
	(45, '111 ha noi', 'Thi Thi', 1, '1999-11-02', '0908780370', NULL, NULL, '2020-08-18 08:21:53.016+00', '2020-08-18 08:21:53.016+00', NULL, 2, '10312343', 78),
	(47, '111 ha noi', 'Thi Thi', 1, '1999-11-02', '0908780370', NULL, NULL, '2020-08-18 08:23:04.863+00', '2020-08-18 08:23:04.863+00', NULL, 2, '103123443', 78),
	(48, '111 ha noi', 'Thi Thi', 1, '1999-11-02', '0908780370', NULL, NULL, '2020-08-18 08:23:26.043+00', '2020-08-18 08:23:26.043+00', NULL, 2, '1031234431', 78),
	(49, '111 ha noi', 'Thi Thi', 1, '1999-11-02', '0908780370', NULL, NULL, '2020-08-18 08:24:17.368+00', '2020-08-18 08:24:17.368+00', NULL, 2, '10312334431', 78),
	(50, '111 ha noi', 'Thi Thi', 1, '1999-11-02', '0908780370', NULL, NULL, '2020-08-18 09:57:40.165+00', '2020-08-18 09:57:40.165+00', NULL, 2, '103123343431', 78),
	(51, '111 ha noi', 'Thi Thi', 1, '1999-11-02', '0908780370', NULL, NULL, '2020-08-18 10:30:29.454+00', '2020-08-18 10:30:29.454+00', NULL, 2, '1031233434311', 78),
	(53, '111 ha noi', 'Thi Thi', 1, '1999-11-02', '0908780370', NULL, NULL, '2020-08-18 10:31:10.694+00', '2020-08-18 10:31:10.694+00', NULL, 2, '10312334343111', 78),
	(55, '111 ha noi', 'Thi Thi', 1, '1999-11-02', '0908780370', NULL, NULL, '2020-08-18 10:33:01.144+00', '2020-08-18 10:33:01.144+00', NULL, 2, '103123343431111', 78),
	(64, '111 ha noi', 'Thi Thi', 1, '1999-11-02', '0908780370', NULL, NULL, '2020-08-19 03:20:51.474+00', '2020-08-19 03:20:51.474+00', NULL, 2, '422113', 78),
	(66, '111 ha noi', 'Thi Thi', 1, '1999-11-02', '0908780370', NULL, NULL, '2020-08-19 03:21:39.92+00', '2020-08-19 03:21:39.92+00', NULL, 2, '4221113', 78),
	(67, '111 ha noi', 'Thi Thi', 1, '1999-11-02', '0908780370', NULL, NULL, '2020-08-19 03:22:07.812+00', '2020-08-19 03:22:07.812+00', NULL, 2, '42211113', 78),
	(68, '111 ha noi', 'Thi Thi', 1, '1999-11-02', '0908780370', NULL, NULL, '2020-08-19 03:22:28.835+00', '2020-08-19 03:22:28.835+00', NULL, 2, '422311113', 78),
	(56, '111 ha noi', 'Thi Thi', 1, '1999-11-02', '0908780370', NULL, NULL, '2020-08-19 03:07:41.231+00', '2020-08-19 03:07:41.231+00', NULL, 2, '42', 78),
	(58, '111 ha noi', 'Thi Thi', 1, '1999-11-02', '0908780370', NULL, NULL, '2020-08-19 03:08:32.733+00', '2020-08-19 03:08:32.733+00', NULL, 2, '421', 78),
	(70, '111 ha noi', 'Thi Thi', 1, '1999-11-02', '0908780370', NULL, NULL, '2020-08-19 03:22:41.155+00', '2020-08-19 03:22:41.155+00', NULL, 2, '4223111113', 78),
	(71, '111 ha noi', 'Thi Thi', 1, '1999-11-02', '0908780370', NULL, NULL, '2020-08-19 03:23:10.027+00', '2020-08-19 03:23:10.027+00', NULL, 2, '42231111113', 78),
	(73, '111 ha noi', 'Thi Thi', 1, '1999-11-02', '0908780370', NULL, NULL, '2020-08-19 03:25:35.15+00', '2020-08-19 03:25:35.15+00', NULL, 2, '422311111113', 78),
	(75, '111 ha noi', 'Thi Thi', 1, '1999-11-02', '0908780370', NULL, NULL, '2020-08-19 03:27:08.356+00', '2020-08-19 03:27:08.356+00', NULL, 2, '4223111111313', 78),
	(76, '111 ha noi', 'Thi Thi', 1, '1999-11-02', '0908780370', NULL, NULL, '2020-08-19 03:29:35.058+00', '2020-08-19 03:29:35.058+00', NULL, 2, '42231111131313', 78),
	(77, '111 ha noi', 'Thi Thi', 1, '1999-11-02', '0908780370', NULL, NULL, '2020-08-19 03:43:16.865+00', '2020-08-19 03:43:16.865+00', NULL, 2, '422311113131313', 78),
	(78, '111 ha noi', 'Thi Thi', 1, '1999-11-02', '0908780370', NULL, NULL, '2020-08-19 03:43:39.905+00', '2020-08-19 03:43:39.905+00', NULL, 2, '4223113113131313', 78),
	(79, '111 ha noi', 'Thi Thi', 1, '1999-11-02', '0908780370', NULL, NULL, '2020-08-19 03:43:59.65+00', '2020-08-19 03:43:59.65+00', NULL, 2, '42231131131331310', 78),
	(80, '111 ha noi', 'Thi Thi', 1, '1999-11-02', '0908780370', NULL, NULL, '2020-08-19 03:44:32.754+00', '2020-08-19 03:44:32.754+00', NULL, 2, '422311311331331300', 78),
	(81, '111 ha noi', 'Thi Thi', 1, '1999-11-02', '0908780370', NULL, NULL, '2020-08-19 03:46:16.597+00', '2020-08-19 03:46:16.597+00', NULL, 2, '4223113113313331000', 78),
	(82, '111 ha noi', 'Thi Thi', 1, '1999-11-02', '0908780370', NULL, NULL, '2020-08-19 04:46:58.336+00', '2020-08-19 04:46:58.336+00', NULL, 2, '1999', 78),
	(83, '111 ha noi', 'Thi Thi', 1, '1999-11-02', '0908780370', NULL, NULL, '2020-08-19 04:47:45.402+00', '2020-08-19 04:47:45.402+00', NULL, 2, '19399', 78),
	(1, '119 sadsd', 'dep  ne', 1, '2900-05-09', '0916622233444444', NULL, NULL, '2020-08-11 07:32:16.52+00', '2020-08-19 09:13:56.988+00', NULL, 2, '123', 65);


--
-- Data for Name: employees; Type: TABLE DATA; Schema: public; Owner: root
--

INSERT INTO public.employees VALUES
	(1, 'thivvnlh@gmail1.com', '$2a$10$T82nOaVdhPOpnn7LJfNNdur6AzUu7LICQ4zU8MEZCLFpsWpiOivbq', 'ho dep trai3', 1, 66, '119 sadsd', 1, '1999-11-02', '090837593', NULL, NULL, '2020-08-14 03:58:02.783', '2020-08-19 09:38:30.367', NULL, 2),
	(2, 'thivvnlh@gmail2.com', '$2a$10$iFT9TZyPW8IFKTo3N4A19.v4O8IRdWsHt4PHmENJN1WdVjzZH8rZO', 'Ho', 1, 66, 'pro chi la annh', 1, '1999-11-02', '090837593', NULL, NULL, '2020-08-14 04:05:00.051', '2020-08-14 04:05:00.135', NULL, 2),
	(3, 'thivvnlh@gmail23.com', '$2a$10$Mz2qLfJBqmiDBDhbu2yjI.2kEjlYf7G6CfzgVj67wRx0ohonxGfXK', 'Ho', 1, 66, 'pro chi la annh', 1, '1999-11-02', '090837593', NULL, NULL, '2020-08-14 07:20:24.882', '2020-08-14 07:20:24.948', NULL, 2),
	(4, 'thivvnlh@gmail233.com', '$2a$10$5eWOy61vbclRiuFKDPG0AeBDHuOXanT.BWkq45bgyeIf2lT1r6K.q', 'Ho', 1, 66, 'pro chi la annh', 1, '1999-11-02', '090837593', NULL, NULL, '2020-08-14 07:21:03.126', '2020-08-14 07:21:03.192', NULL, 2),
	(5, 'thivvnlh@gmail2133.com', '$2a$10$kLx/q0YLf6sE3ZVfkKQ8PuvVBDqPeDpi9r6nMk8GCaNzl27euny0K', 'Ho', 1, 66, 'pro chi la annh', 1, '1999-11-02', '090837593', NULL, NULL, '2020-08-19 08:25:34.959', '2020-08-19 08:25:35.048', NULL, 2),
	(6, 'thivvnlh@g2mail2133.com', '$2a$10$Bpuq6Bkmcd5SDFZD.Y5UXOOvwtXta4pkYB..kOEVqbYslkRClBmNu', 'Ho', 1, 66, 'pro chi la annh', 1, '1999-11-02', '090837593', NULL, NULL, '2020-08-19 08:26:20.667', '2020-08-19 08:26:20.758', NULL, 2),
	(7, 'thivvnlh@g2mail22133.com', '$2a$10$1l9HaQxw0StjCOyMbLD32O/vdQd8S0iCeqLrhMOoBWfyJxTDpJom2', 'Ho', 1, 66, 'pro chi la annh', 1, '1999-11-02', '090837593', NULL, NULL, '2020-08-19 08:28:04.045', '2020-08-19 08:28:04.136', NULL, 2),
	(8, 'thivvnlh@g2mai2l22133.com', '$2a$10$bWdKX6coT2hOKzmeqkw1Y.BMBx2g36GT9XhJYnTftt59GJK0.djai', 'Ho', 1, 66, 'pro chi la annh', 1, '1999-11-02', '090837593', NULL, NULL, '2020-08-19 08:50:38.214', '2020-08-19 08:50:38.303', NULL, 2);


--
-- Data for Name: locations; Type: TABLE DATA; Schema: public; Owner: root
--

INSERT INTO public.locations VALUES
	(66, 'Châu Phú', NULL, NULL, NULL, 2, 1),
	(67, 'Châu Thành', NULL, NULL, NULL, 2, 1),
	(112, 'Thuận Thành', NULL, NULL, NULL, 2, 6),
	(68, 'Chợ Mới', NULL, NULL, NULL, 2, 1),
	(69, 'TP. Long Xuyên', NULL, NULL, NULL, 2, 1),
	(70, 'Phú Tân', NULL, NULL, NULL, 2, 1),
	(71, 'TX. Tân Châu', NULL, NULL, NULL, 2, 1),
	(72, 'Thoại Sơn', NULL, NULL, NULL, 2, 1),
	(73, 'Tịnh Biên', NULL, NULL, NULL, 2, 1),
	(93, 'Ba Bể', NULL, NULL, NULL, 2, 4),
	(94, 'Bạch Thông', NULL, NULL, NULL, 2, 4),
	(74, 'Tri Tôn', NULL, NULL, NULL, 2, 1),
	(75, 'HĐ. Côn Đảo', NULL, NULL, NULL, 2, 2),
	(95, 'TP. Bắc Kạn', NULL, NULL, NULL, 2, 4),
	(96, 'Chợ Đồn', NULL, NULL, NULL, 2, 4),
	(97, 'Chợ Mới', NULL, NULL, NULL, 2, 4),
	(76, 'Đất Đỏ', NULL, NULL, NULL, 2, 2),
	(98, 'Na Rì', NULL, NULL, NULL, 2, 4),
	(99, 'Ngân Sơn', NULL, NULL, NULL, 2, 4),
	(77, 'Tân Thành', NULL, NULL, NULL, 2, 2),
	(2, 'Bà Rịa - Vũng Tàu', NULL, NULL, NULL, 1, NULL),
	(3, 'Bắc Giang', NULL, NULL, NULL, 1, NULL),
	(4, 'Bắc Kạn', NULL, NULL, NULL, 1, NULL),
	(5, 'Bạc Liêu', NULL, NULL, NULL, 1, NULL),
	(6, 'Bắc Ninh', NULL, NULL, NULL, 1, NULL),
	(78, 'TP. Vũng Tàu', NULL, NULL, NULL, 2, 2),
	(8, 'Bình Định', NULL, NULL, NULL, 1, NULL),
	(7, 'Bến Tre', NULL, NULL, NULL, 1, NULL),
	(9, 'Bình Dương', NULL, NULL, NULL, 1, NULL),
	(10, 'Bình Phước', NULL, NULL, NULL, 1, NULL),
	(11, 'Bình Thuận', NULL, NULL, NULL, 1, NULL),
	(12, 'Cà Mau', NULL, NULL, NULL, 1, NULL),
	(13, 'Cần Thơ', NULL, NULL, NULL, 1, NULL),
	(14, 'Cao Bằng', NULL, NULL, NULL, 1, NULL),
	(15, 'Đà Nẵng', NULL, NULL, NULL, 1, NULL),
	(16, 'Đắk Lắk', NULL, NULL, NULL, 1, NULL),
	(17, 'Đăk Nông', NULL, NULL, NULL, 1, NULL),
	(18, 'Điện Biên', NULL, NULL, NULL, 1, NULL),
	(19, 'Đồng Nai', NULL, NULL, NULL, 1, NULL),
	(20, 'Đồng Tháp', NULL, NULL, NULL, 1, NULL),
	(21, 'Gia Lai', NULL, NULL, NULL, 1, NULL),
	(22, 'Hà Giang', NULL, NULL, NULL, 1, NULL),
	(23, 'Hà Nam', NULL, NULL, NULL, 1, NULL),
	(24, 'Hà Nội', NULL, NULL, NULL, 1, NULL),
	(25, 'Hà Tĩnh', NULL, NULL, NULL, 1, NULL),
	(26, 'Hải Dương', NULL, NULL, NULL, 1, NULL),
	(27, 'Hải Phòng', NULL, NULL, NULL, 1, NULL),
	(28, 'Hậu Giang', NULL, NULL, NULL, 1, NULL),
	(29, 'Hoà Bình', NULL, NULL, NULL, 1, NULL),
	(30, 'Hưng Yên', NULL, NULL, NULL, 1, NULL),
	(31, 'Khánh Hòa', NULL, NULL, NULL, 1, NULL),
	(32, 'Kiên Giang', NULL, NULL, NULL, 1, NULL),
	(33, 'Kon Tum', NULL, NULL, NULL, 1, NULL),
	(34, 'Lai Châu', NULL, NULL, NULL, 1, NULL),
	(35, 'Lâm Đồng', NULL, NULL, NULL, 1, NULL),
	(36, 'Lạng Sơn', NULL, NULL, NULL, 1, NULL),
	(113, 'Tiên Du', NULL, NULL, NULL, 2, 6),
	(37, 'Lào Cai', NULL, NULL, NULL, 1, NULL),
	(38, 'Long An', NULL, NULL, NULL, 1, NULL),
	(79, 'Xuyên Mộc', NULL, NULL, NULL, 2, 2),
	(39, 'Nam Định', NULL, NULL, NULL, 1, NULL),
	(40, 'Nghệ An', NULL, NULL, NULL, 1, NULL),
	(41, 'Ninh Bình', NULL, NULL, NULL, 1, NULL),
	(80, 'TP. Bà Rịa', NULL, NULL, NULL, 2, 2),
	(42, 'Ninh Thuận', NULL, NULL, NULL, 1, NULL),
	(43, 'Phú Thọ', NULL, NULL, NULL, 1, NULL),
	(44, 'Phú Yên', NULL, NULL, NULL, 1, NULL),
	(81, 'Châu Đức', NULL, NULL, NULL, 2, 2),
	(45, 'Quảng Bình', NULL, NULL, NULL, 1, NULL),
	(46, 'Quảng Nam', NULL, NULL, NULL, 1, NULL),
	(47, 'Quảng Ngãi', NULL, NULL, NULL, 1, NULL),
	(48, 'Quảng Ninh', NULL, NULL, NULL, 1, NULL),
	(49, 'Quảng Trị', NULL, NULL, NULL, 1, NULL),
	(50, 'Sóc Trăng', NULL, NULL, NULL, 1, NULL),
	(51, 'Sơn La', NULL, NULL, NULL, 1, NULL),
	(52, 'Tây Ninh', NULL, NULL, NULL, 1, NULL),
	(53, 'Thái Bình', NULL, NULL, NULL, 1, NULL),
	(54, 'Thái Nguyên', NULL, NULL, NULL, 1, NULL),
	(114, 'TX. Từ Sơn', NULL, NULL, NULL, 2, 6),
	(101, 'TP. Bạc Liêu', NULL, NULL, NULL, 2, 5),
	(55, 'Thanh Hóa', NULL, NULL, NULL, 1, NULL),
	(56, 'Thừa Thiên - Huế', NULL, NULL, NULL, 1, NULL),
	(57, 'Tiền Giang', NULL, NULL, NULL, 1, NULL),
	(58, 'TP Hồ Chí Minh', NULL, NULL, NULL, 1, NULL),
	(59, 'Trà Vinh', NULL, NULL, NULL, 1, NULL),
	(60, 'Tuyên Quang', NULL, NULL, NULL, 1, NULL),
	(61, 'Vĩnh Long', NULL, NULL, NULL, 1, NULL),
	(62, 'Vĩnh Phúc', NULL, NULL, NULL, 1, NULL),
	(82, 'Long Điền', NULL, NULL, NULL, 2, 2),
	(63, 'Yên Bái', NULL, NULL, NULL, 1, NULL),
	(83, 'TP. Bắc Giang', NULL, NULL, NULL, 2, 3),
	(84, 'Hiệp Hòa', NULL, NULL, NULL, 2, 3),
	(64, 'An Phú', NULL, NULL, NULL, 2, 1),
	(85, 'Lạng Giang', NULL, NULL, NULL, 2, 3),
	(86, 'Lục Nam', NULL, NULL, NULL, 2, 3),
	(65, 'TP. Châu Đốc', NULL, NULL, NULL, 2, 1),
	(87, 'Lục Ngạn', NULL, NULL, NULL, 2, 3),
	(88, 'Sơn Động', NULL, NULL, NULL, 2, 3),
	(89, 'Tân Yên', NULL, NULL, NULL, 2, 3),
	(102, 'Đông Hải', NULL, NULL, NULL, 2, 5),
	(103, 'TX. Giá Rai', NULL, NULL, NULL, 2, 5),
	(90, 'Việt Yên', NULL, NULL, NULL, 2, 3),
	(91, 'Yên Dũng', NULL, NULL, NULL, 2, 3),
	(104, 'Hoà Bình', NULL, NULL, NULL, 2, 5),
	(92, 'Yên Thế', NULL, NULL, NULL, 2, 3),
	(105, 'Hồng Dân', NULL, NULL, NULL, 2, 5),
	(106, 'Phước Long', NULL, NULL, NULL, 2, 5),
	(107, 'Vĩnh Lợi', NULL, NULL, NULL, 2, 5),
	(108, 'TP. Bắc Ninh', NULL, NULL, NULL, 2, 6),
	(109, 'Gia Bình', NULL, NULL, NULL, 2, 6),
	(110, 'Lương Tài', NULL, NULL, NULL, 2, 6),
	(111, 'Quế Võ', NULL, NULL, NULL, 2, 6),
	(115, 'Yên Phong', NULL, NULL, NULL, 2, 6),
	(116, 'Ba Tri', NULL, NULL, NULL, 2, 7),
	(117, 'TP. Bến Tre', NULL, NULL, NULL, 2, 7),
	(118, 'Bình Đại', NULL, NULL, NULL, 2, 7),
	(119, 'Châu Thành', NULL, NULL, NULL, 2, 7),
	(120, 'Chợ Lách', NULL, NULL, NULL, 2, 7),
	(121, 'Giồng Trôm', NULL, NULL, NULL, 2, 7),
	(122, 'Mỏ Cày Bắc', NULL, NULL, NULL, 2, 7),
	(123, 'Mỏ Cày Nam', NULL, NULL, NULL, 2, 7),
	(124, 'Thạnh Phú', NULL, NULL, NULL, 2, 7),
	(125, 'An Lão', '2020-08-10 17:42:20.217+00', '2020-08-10 17:42:20.217+00', NULL, 2, 8),
	(126, 'TX. An Nhơn', '2020-08-10 18:00:45.379+00', '2020-08-10 18:00:45.379+00', NULL, 2, 8),
	(127, 'Hoài Ân', '2020-08-10 18:01:37.083+00', '2020-08-10 18:01:37.083+00', NULL, 2, 8),
	(128, 'TX. Hoài Nhơn', '2020-08-10 18:01:37.083+00', '2020-08-10 18:01:37.083+00', NULL, 2, 8),
	(129, 'Phù Cát', '2020-08-10 18:01:37.083+00', '2020-08-10 18:01:37.083+00', NULL, 2, 8),
	(130, 'Phù Mỹ', '2020-08-10 18:01:37.084+00', '2020-08-10 18:01:37.084+00', NULL, 2, 8),
	(131, 'TP. Quy Nhơn', '2020-08-10 18:01:37.084+00', '2020-08-10 18:01:37.084+00', NULL, 2, 8),
	(132, 'Tây Sơn', '2020-08-10 18:01:37.084+00', '2020-08-10 18:01:37.084+00', NULL, 2, 8),
	(133, 'Tuy Phước', '2020-08-10 18:01:37.084+00', '2020-08-10 18:01:37.084+00', NULL, 2, 8),
	(134, 'Vân Canh', '2020-08-10 18:01:37.084+00', '2020-08-10 18:01:37.084+00', NULL, 2, 8),
	(135, 'Vĩnh Thạnh', '2020-08-10 18:01:37.084+00', '2020-08-10 18:01:37.084+00', NULL, 2, 8),
	(136, 'Bàu Bàng', '2020-08-10 18:14:32.076+00', '2020-08-10 18:14:32.076+00', NULL, 2, 9),
	(137, 'Bắc Tân Uyên', '2020-08-10 18:15:49.31+00', '2020-08-10 18:15:49.31+00', NULL, 2, 9),
	(138, 'TX. Bến Cát', '2020-08-10 18:15:49.311+00', '2020-08-10 18:15:49.311+00', NULL, 2, 9),
	(139, 'Dầu Tiếng', '2020-08-10 18:15:49.311+00', '2020-08-10 18:15:49.311+00', NULL, 2, 9),
	(140, 'TX. Dĩ An', '2020-08-10 18:15:49.312+00', '2020-08-10 18:15:49.312+00', NULL, 2, 9),
	(141, 'Phú Giáo', '2020-08-10 18:15:49.312+00', '2020-08-10 18:15:49.312+00', NULL, 2, 9),
	(142, 'TP. Thủ Dầu Một', '2020-08-10 18:15:49.312+00', '2020-08-10 18:15:49.312+00', NULL, 2, 9),
	(143, 'TX. Tân Uyên', '2020-08-10 18:15:49.312+00', '2020-08-10 18:15:49.312+00', NULL, 2, 9),
	(144, 'TX. Thuận An', '2020-08-10 18:15:49.312+00', '2020-08-10 18:15:49.312+00', NULL, 2, 9),
	(145, 'TX. Bình Long', '2020-08-10 18:15:49.312+00', '2020-08-10 18:15:49.312+00', NULL, 2, 10),
	(146, 'Bù Đăng', '2020-08-10 18:15:49.313+00', '2020-08-10 18:15:49.313+00', NULL, 2, 10),
	(147, 'Bù Đốp', '2020-08-10 18:15:49.313+00', '2020-08-10 18:15:49.313+00', NULL, 2, 10),
	(148, 'Bù Gia Mập', '2020-08-10 18:15:49.313+00', '2020-08-10 18:15:49.313+00', NULL, 2, 10),
	(149, 'Chơn Thành', '2020-08-10 18:15:49.313+00', '2020-08-10 18:15:49.313+00', NULL, 2, 10),
	(150, 'Đồng Phú', '2020-08-10 18:15:49.313+00', '2020-08-10 18:15:49.313+00', NULL, 2, 10),
	(151, 'TX. Đồng Xoài', '2020-08-10 18:15:49.313+00', '2020-08-10 18:15:49.313+00', NULL, 2, 10),
	(152, 'Hớn Quản', '2020-08-10 18:15:49.313+00', '2020-08-10 18:15:49.313+00', NULL, 2, 10),
	(153, 'Lộc Ninh', '2020-08-10 18:15:49.313+00', '2020-08-10 18:15:49.313+00', NULL, 2, 10),
	(154, 'Phú Riềng', '2020-08-10 18:15:49.314+00', '2020-08-10 18:15:49.314+00', NULL, 2, 10),
	(155, 'TX. Phước Long', '2020-08-10 18:15:49.314+00', '2020-08-10 18:15:49.314+00', NULL, 2, 10),
	(156, 'Bắc Bình', '2020-08-10 18:15:49.314+00', '2020-08-10 18:15:49.314+00', NULL, 2, 11),
	(157, 'Đức Linh', '2020-08-10 18:15:49.314+00', '2020-08-10 18:15:49.314+00', NULL, 2, 11),
	(158, 'Hàm Tân', '2020-08-10 18:15:49.314+00', '2020-08-10 18:15:49.314+00', NULL, 2, 11),
	(159, 'Hàm Thuận Bắc', '2020-08-10 18:15:49.314+00', '2020-08-10 18:15:49.314+00', NULL, 2, 11),
	(160, 'Hàm Thuận Nam', '2020-08-10 18:15:49.314+00', '2020-08-10 18:15:49.314+00', NULL, 2, 11),
	(161, 'TX. La Gi', '2020-08-10 18:15:49.314+00', '2020-08-10 18:15:49.314+00', NULL, 2, 11),
	(162, 'TP. Phan Thiết', '2020-08-10 18:15:49.315+00', '2020-08-10 18:15:49.315+00', NULL, 2, 11),
	(163, 'HĐ. Phú Quý', '2020-08-10 18:15:49.315+00', '2020-08-10 18:15:49.315+00', NULL, 2, 11),
	(164, 'Tánh Linh', '2020-08-10 18:15:49.315+00', '2020-08-10 18:15:49.315+00', NULL, 2, 11),
	(165, 'Tuy Phong', '2020-08-10 18:15:49.315+00', '2020-08-10 18:15:49.315+00', NULL, 2, 11),
	(166, 'TP. Cà Mau', '2020-08-10 18:15:49.315+00', '2020-08-10 18:15:49.315+00', NULL, 2, 12),
	(167, 'Cái Nước', '2020-08-10 18:15:49.315+00', '2020-08-10 18:15:49.315+00', NULL, 2, 12),
	(168, 'Đầm Dơi', '2020-08-10 18:15:49.315+00', '2020-08-10 18:15:49.315+00', NULL, 2, 12),
	(169, 'Năm Căn', '2020-08-10 18:15:49.315+00', '2020-08-10 18:15:49.315+00', NULL, 2, 12),
	(170, 'Ngọc Hiển', '2020-08-10 18:15:49.316+00', '2020-08-10 18:15:49.316+00', NULL, 2, 12),
	(171, 'Phú Tân', '2020-08-10 18:15:49.316+00', '2020-08-10 18:15:49.316+00', NULL, 2, 12),
	(172, 'Thới Bình', '2020-08-10 18:15:49.316+00', '2020-08-10 18:15:49.316+00', NULL, 2, 12),
	(173, 'Trần Văn Thời', '2020-08-10 18:15:49.316+00', '2020-08-10 18:15:49.316+00', NULL, 2, 12),
	(174, 'U Minh', '2020-08-10 18:15:49.316+00', '2020-08-10 18:15:49.316+00', NULL, 2, 12),
	(175, 'Q. Bình Thủy', '2020-08-10 18:15:49.316+00', '2020-08-10 18:15:49.316+00', NULL, 2, 13),
	(176, 'Q. Cái Răng', '2020-08-10 18:15:49.316+00', '2020-08-10 18:15:49.316+00', NULL, 2, 13),
	(177, 'Cờ Đỏ', '2020-08-10 18:15:49.316+00', '2020-08-10 18:15:49.316+00', NULL, 2, 13),
	(178, 'Q. Ninh Kiều', '2020-08-10 18:15:49.317+00', '2020-08-10 18:15:49.317+00', NULL, 2, 13),
	(179, 'Q. Ô Môn', '2020-08-10 18:15:49.317+00', '2020-08-10 18:15:49.317+00', NULL, 2, 13),
	(180, 'Phong Điền', '2020-08-10 18:15:49.317+00', '2020-08-10 18:15:49.317+00', NULL, 2, 13),
	(181, 'Q. Thốt Nốt', '2020-08-10 18:15:49.317+00', '2020-08-10 18:15:49.317+00', NULL, 2, 13),
	(182, 'Thới Lai', '2020-08-10 18:15:49.317+00', '2020-08-10 18:15:49.317+00', NULL, 2, 13),
	(183, 'Vĩnh Thạnh', '2020-08-10 18:15:49.317+00', '2020-08-10 18:15:49.317+00', NULL, 2, 13),
	(184, 'Bảo Lạc', '2020-08-10 18:15:49.317+00', '2020-08-10 18:15:49.317+00', NULL, 2, 14),
	(185, 'Bảo Lâm', '2020-08-10 18:15:49.317+00', '2020-08-10 18:15:49.317+00', NULL, 2, 14),
	(186, 'TP. Cao Bằng', '2020-08-10 18:15:49.318+00', '2020-08-10 18:15:49.318+00', NULL, 2, 14),
	(187, 'Hà Quảng', '2020-08-10 18:15:49.318+00', '2020-08-10 18:15:49.318+00', NULL, 2, 14),
	(188, 'Hạ Lang', '2020-08-10 18:15:49.318+00', '2020-08-10 18:15:49.318+00', NULL, 2, 14),
	(189, 'Hòa An', '2020-08-10 18:15:49.318+00', '2020-08-10 18:15:49.318+00', NULL, 2, 14),
	(190, 'Nguyên Bình', '2020-08-10 18:15:49.318+00', '2020-08-10 18:15:49.318+00', NULL, 2, 14),
	(191, 'Phục Hòa', '2020-08-10 18:15:49.318+00', '2020-08-10 18:15:49.318+00', NULL, 2, 14),
	(192, 'Quảng Uyên', '2020-08-10 18:15:49.318+00', '2020-08-10 18:15:49.318+00', NULL, 2, 14),
	(193, 'Thạch An', '2020-08-10 18:15:49.318+00', '2020-08-10 18:15:49.318+00', NULL, 2, 14),
	(194, 'Thông Nông', '2020-08-10 18:15:49.319+00', '2020-08-10 18:15:49.319+00', NULL, 2, 14),
	(195, 'Trà Lĩnh', '2020-08-10 18:15:49.319+00', '2020-08-10 18:15:49.319+00', NULL, 2, 14),
	(196, 'Trùng Khánh', '2020-08-10 18:15:49.319+00', '2020-08-10 18:15:49.319+00', NULL, 2, 14),
	(197, 'Q. Cẩm Lệ', '2020-08-10 18:15:49.319+00', '2020-08-10 18:15:49.319+00', NULL, 2, 15),
	(198, 'Q. Hải Châu', '2020-08-10 18:15:49.319+00', '2020-08-10 18:15:49.319+00', NULL, 2, 15),
	(199, 'Hòa Vang', '2020-08-10 18:15:49.319+00', '2020-08-10 18:15:49.319+00', NULL, 2, 15),
	(200, 'HĐ. Hoàng Sa', '2020-08-10 18:15:49.319+00', '2020-08-10 18:15:49.319+00', NULL, 2, 15),
	(201, 'Q. Liên Chiểu', '2020-08-10 18:15:49.319+00', '2020-08-10 18:15:49.319+00', NULL, 2, 15),
	(202, 'Q. Ngũ Hành Sơn', '2020-08-10 18:15:49.32+00', '2020-08-10 18:15:49.32+00', NULL, 2, 15),
	(203, 'Q. Sơn Trà', '2020-08-10 18:15:49.32+00', '2020-08-10 18:15:49.32+00', NULL, 2, 15),
	(204, 'Q. Thanh Khê', '2020-08-10 18:15:49.32+00', '2020-08-10 18:15:49.32+00', NULL, 2, 15),
	(205, 'Buôn Đôn', '2020-08-10 18:15:49.32+00', '2020-08-10 18:15:49.32+00', NULL, 2, 16),
	(206, 'TX. Buôn Hồ', '2020-08-10 18:15:49.32+00', '2020-08-10 18:15:49.32+00', NULL, 2, 16),
	(207, 'TP. Buôn Ma Thuột', '2020-08-10 18:15:49.32+00', '2020-08-10 18:15:49.32+00', NULL, 2, 16),
	(208, 'Cư Kuin', '2020-08-10 18:15:49.32+00', '2020-08-10 18:15:49.32+00', NULL, 2, 16),
	(209, 'Cư M''gar', '2020-08-10 18:15:49.32+00', '2020-08-10 18:15:49.32+00', NULL, 2, 16),
	(210, 'Ea H''leo', '2020-08-10 18:15:49.32+00', '2020-08-10 18:15:49.32+00', NULL, 2, 16),
	(211, 'Ea Kar', '2020-08-10 18:15:49.321+00', '2020-08-10 18:15:49.321+00', NULL, 2, 16),
	(216, 'Krông Năng', '2020-08-10 18:15:49.321+00', '2020-08-10 18:15:49.321+00', NULL, 2, 16),
	(219, 'M''Đrăk', '2020-08-10 18:15:49.322+00', '2020-08-10 18:15:49.322+00', NULL, 2, 16),
	(224, 'Đắk Mil', '2020-08-10 18:15:49.322+00', '2020-08-10 18:15:49.322+00', NULL, 2, 17),
	(212, 'Ea Súp', '2020-08-10 18:15:49.321+00', '2020-08-10 18:15:49.321+00', NULL, 2, 16),
	(217, 'Krông Pắk', '2020-08-10 18:15:49.321+00', '2020-08-10 18:15:49.321+00', NULL, 2, 16),
	(220, 'TX. Gia Nghĩa', '2020-08-10 18:15:49.322+00', '2020-08-10 18:15:49.322+00', NULL, 2, 17),
	(227, 'Krông Nô', '2020-08-10 18:15:49.323+00', '2020-08-10 18:15:49.323+00', NULL, 2, 17),
	(213, 'Krông Ana', '2020-08-10 18:15:49.321+00', '2020-08-10 18:15:49.321+00', NULL, 2, 16),
	(218, 'Lắk', '2020-08-10 18:15:49.321+00', '2020-08-10 18:15:49.321+00', NULL, 2, 16),
	(223, 'Đắk Glong', '2020-08-10 18:15:49.322+00', '2020-08-10 18:15:49.322+00', NULL, 2, 17),
	(225, 'Đắk R''lấp', '2020-08-10 18:15:49.322+00', '2020-08-10 18:15:49.322+00', NULL, 2, 17),
	(214, 'Krông Bông', '2020-08-10 18:15:49.321+00', '2020-08-10 18:15:49.321+00', NULL, 2, 16),
	(222, 'Cư Jút', '2020-08-10 18:15:49.322+00', '2020-08-10 18:15:49.322+00', NULL, 2, 17),
	(226, 'Đăk Song', '2020-08-10 18:15:49.322+00', '2020-08-10 18:15:49.322+00', NULL, 2, 17),
	(215, 'Krông Búk', '2020-08-10 18:15:49.321+00', '2020-08-10 18:15:49.321+00', NULL, 2, 16),
	(221, 'Tuy Đức', '2020-08-10 18:15:49.322+00', '2020-08-10 18:15:49.322+00', NULL, 2, 17),
	(229, 'Điện Biên', '2020-08-10 18:29:17.065+00', '2020-08-10 18:29:17.065+00', NULL, 2, 18),
	(231, 'Mường Ảng', '2020-08-10 18:29:17.066+00', '2020-08-10 18:29:17.066+00', NULL, 2, 18),
	(232, 'Mường Chà', '2020-08-10 18:29:17.066+00', '2020-08-10 18:29:17.066+00', NULL, 2, 18),
	(233, 'TX. Mường Lay', '2020-08-10 18:29:17.066+00', '2020-08-10 18:29:17.066+00', NULL, 2, 18),
	(234, 'Mường Nhé', '2020-08-10 18:29:17.066+00', '2020-08-10 18:29:17.066+00', NULL, 2, 18),
	(235, 'Nậm Pồ', '2020-08-10 18:29:17.066+00', '2020-08-10 18:29:17.066+00', NULL, 2, 18),
	(236, 'Tủa Chùa', '2020-08-10 18:29:17.066+00', '2020-08-10 18:29:17.066+00', NULL, 2, 18),
	(237, 'Tuần Giáo', '2020-08-10 18:29:17.066+00', '2020-08-10 18:29:17.066+00', NULL, 2, 18),
	(238, 'TP. Biên Hòa', '2020-08-10 18:29:17.066+00', '2020-08-10 18:29:17.066+00', NULL, 2, 19),
	(239, 'Cẩm Mỹ', '2020-08-10 18:29:17.067+00', '2020-08-10 18:29:17.067+00', NULL, 2, 19),
	(240, 'Định Quán', '2020-08-10 18:29:17.067+00', '2020-08-10 18:29:17.067+00', NULL, 2, 19),
	(241, 'TX. Long Khánh', '2020-08-10 18:29:17.067+00', '2020-08-10 18:29:17.067+00', NULL, 2, 19),
	(242, 'Long Thành', '2020-08-10 18:29:17.067+00', '2020-08-10 18:29:17.067+00', NULL, 2, 19),
	(243, 'Nhơn Trạch', '2020-08-10 18:29:17.067+00', '2020-08-10 18:29:17.067+00', NULL, 2, 19),
	(244, 'Tân Phú', '2020-08-10 18:29:17.067+00', '2020-08-10 18:29:17.067+00', NULL, 2, 19),
	(245, 'Thống Nhất', '2020-08-10 18:29:17.067+00', '2020-08-10 18:29:17.067+00', NULL, 2, 19),
	(246, 'Trảng Bom', '2020-08-10 18:29:17.067+00', '2020-08-10 18:29:17.067+00', NULL, 2, 19),
	(247, 'Vĩnh Cửu', '2020-08-10 18:29:17.067+00', '2020-08-10 18:29:17.067+00', NULL, 2, 19),
	(248, 'Xuân Lộc', '2020-08-10 18:29:17.067+00', '2020-08-10 18:29:17.067+00', NULL, 2, 19),
	(249, 'TP. Cao Lãnh', '2020-08-10 18:29:17.067+00', '2020-08-10 18:29:17.067+00', NULL, 2, 20),
	(250, 'Cao Lãnh', '2020-08-10 18:29:17.067+00', '2020-08-10 18:29:17.067+00', NULL, 2, 20),
	(251, 'Châu Thành', '2020-08-10 18:29:17.067+00', '2020-08-10 18:29:17.067+00', NULL, 2, 20),
	(253, 'TX. Hồng Ngự', '2020-08-10 18:29:17.068+00', '2020-08-10 18:29:17.068+00', NULL, 2, 20),
	(254, 'Lai Vung', '2020-08-10 18:29:17.068+00', '2020-08-10 18:29:17.068+00', NULL, 2, 20),
	(255, 'Lấp Vò', '2020-08-10 18:29:17.068+00', '2020-08-10 18:29:17.068+00', NULL, 2, 20);
INSERT INTO public.locations VALUES
	(256, 'TP. Sa Đéc', '2020-08-10 18:29:17.068+00', '2020-08-10 18:29:17.068+00', NULL, 2, 20),
	(257, 'Tam Nông', '2020-08-10 18:29:17.068+00', '2020-08-10 18:29:17.068+00', NULL, 2, 20),
	(258, 'Tân Hồng', '2020-08-10 18:29:17.068+00', '2020-08-10 18:29:17.068+00', NULL, 2, 20),
	(259, 'Thanh Bình', '2020-08-10 18:29:17.068+00', '2020-08-10 18:29:17.068+00', NULL, 2, 20),
	(260, 'Tháp Mười', '2020-08-10 18:29:17.068+00', '2020-08-10 18:29:17.068+00', NULL, 2, 20),
	(261, 'TX. An Khê', '2020-08-10 18:29:17.068+00', '2020-08-10 18:29:17.068+00', NULL, 2, 21),
	(262, 'TX. Ayun Pa', '2020-08-10 18:29:17.068+00', '2020-08-10 18:29:17.068+00', NULL, 2, 21),
	(263, 'Chư Păh', '2020-08-10 18:29:17.068+00', '2020-08-10 18:29:17.068+00', NULL, 2, 21),
	(264, 'Chư Prông', '2020-08-10 18:29:17.068+00', '2020-08-10 18:29:17.068+00', NULL, 2, 21),
	(265, 'Chư Pưh', '2020-08-10 18:29:17.069+00', '2020-08-10 18:29:17.069+00', NULL, 2, 21),
	(266, 'Chư Sê', '2020-08-10 18:29:17.069+00', '2020-08-10 18:29:17.069+00', NULL, 2, 21),
	(267, 'Đắk Đoa', '2020-08-10 18:29:17.069+00', '2020-08-10 18:29:17.069+00', NULL, 2, 21),
	(268, 'Đak Pơ', '2020-08-10 18:29:17.069+00', '2020-08-10 18:29:17.069+00', NULL, 2, 21),
	(269, 'Đức Cơ', '2020-08-10 18:29:17.069+00', '2020-08-10 18:29:17.069+00', NULL, 2, 21),
	(270, 'Ia Grai', '2020-08-10 18:29:17.069+00', '2020-08-10 18:29:17.069+00', NULL, 2, 21),
	(271, 'Ia Pa', '2020-08-10 18:29:17.069+00', '2020-08-10 18:29:17.069+00', NULL, 2, 21),
	(272, 'KBang', '2020-08-10 18:29:17.069+00', '2020-08-10 18:29:17.069+00', NULL, 2, 21),
	(274, 'Krông Pa', '2020-08-10 18:29:17.069+00', '2020-08-10 18:29:17.069+00', NULL, 2, 21),
	(275, 'Mang Yang', '2020-08-10 18:29:17.069+00', '2020-08-10 18:29:17.069+00', NULL, 2, 21),
	(276, 'Phú Thiện', '2020-08-10 18:29:17.069+00', '2020-08-10 18:29:17.069+00', NULL, 2, 21),
	(277, 'TP. Pleiku', '2020-08-10 18:29:17.069+00', '2020-08-10 18:29:17.069+00', NULL, 2, 21),
	(278, 'Bắc Mê', '2020-08-10 18:29:17.069+00', '2020-08-10 18:29:17.069+00', NULL, 2, 22),
	(280, 'Đồng Văn', '2020-08-10 18:29:17.07+00', '2020-08-10 18:29:17.07+00', NULL, 2, 22),
	(281, 'TP. Hà Giang', '2020-08-10 18:29:17.07+00', '2020-08-10 18:29:17.07+00', NULL, 2, 22),
	(279, 'Bắc Quang', '2020-08-10 18:29:17.069+00', '2020-08-10 18:29:17.069+00', NULL, 2, 22),
	(282, 'Hoàng Su Phì', '2020-08-10 18:29:17.07+00', '2020-08-10 18:29:17.07+00', NULL, 2, 22),
	(283, 'Mèo Vạc', '2020-08-10 18:29:17.07+00', '2020-08-10 18:29:17.07+00', NULL, 2, 22),
	(284, 'Quản Bạ', '2020-08-10 18:29:17.07+00', '2020-08-10 18:29:17.07+00', NULL, 2, 22),
	(285, 'Quang Bình', '2020-08-10 18:29:17.07+00', '2020-08-10 18:29:17.07+00', NULL, 2, 22),
	(286, 'Vị Xuyên', '2020-08-10 18:29:17.071+00', '2020-08-10 18:29:17.071+00', NULL, 2, 22),
	(287, 'Xín Mần', '2020-08-10 18:29:17.071+00', '2020-08-10 18:29:17.071+00', NULL, 2, 22),
	(288, 'Yên Minh', '2020-08-10 18:29:17.071+00', '2020-08-10 18:29:17.071+00', NULL, 2, 22),
	(289, 'Bình Lục', '2020-08-10 18:29:17.071+00', '2020-08-10 18:29:17.071+00', NULL, 2, 23),
	(291, 'Kim Bảng', '2020-08-10 18:29:17.071+00', '2020-08-10 18:29:17.071+00', NULL, 2, 23),
	(292, 'Lý Nhân', '2020-08-10 18:29:17.071+00', '2020-08-10 18:29:17.071+00', NULL, 2, 23),
	(293, 'TP. Phủ Lý', '2020-08-10 18:29:17.071+00', '2020-08-10 18:29:17.071+00', NULL, 2, 23),
	(294, 'Thanh Liêm', '2020-08-10 18:29:17.071+00', '2020-08-10 18:29:17.071+00', NULL, 2, 23),
	(295, 'Q. Ba Đình', '2020-08-10 18:29:17.071+00', '2020-08-10 18:29:17.071+00', NULL, 2, 24),
	(296, 'Ba Vì', '2020-08-10 18:29:17.071+00', '2020-08-10 18:29:17.071+00', NULL, 2, 24),
	(297, 'Q. Bắc Từ Liêm', '2020-08-10 18:29:17.071+00', '2020-08-10 18:29:17.071+00', NULL, 2, 24),
	(298, 'Q. Cầu Giấy', '2020-08-10 18:29:17.071+00', '2020-08-10 18:29:17.071+00', NULL, 2, 24),
	(299, 'Chương Mỹ', '2020-08-10 18:29:17.071+00', '2020-08-10 18:29:17.071+00', NULL, 2, 24),
	(300, 'Đan Phượng', '2020-08-10 18:29:17.071+00', '2020-08-10 18:29:17.071+00', NULL, 2, 24),
	(301, 'Đông Anh', '2020-08-10 18:29:17.071+00', '2020-08-10 18:29:17.071+00', NULL, 2, 24),
	(302, 'Q. Đống Đa', '2020-08-10 18:29:17.071+00', '2020-08-10 18:29:17.071+00', NULL, 2, 24),
	(303, 'Gia Lâm', '2020-08-10 18:29:17.071+00', '2020-08-10 18:29:17.071+00', NULL, 2, 24),
	(304, 'Q. Hà Đông', '2020-08-10 18:29:17.072+00', '2020-08-10 18:29:17.072+00', NULL, 2, 24),
	(306, 'Hoài Đức', '2020-08-10 18:29:17.072+00', '2020-08-10 18:29:17.072+00', NULL, 2, 24),
	(307, 'Q. Hoàn Kiếm', '2020-08-10 18:29:17.072+00', '2020-08-10 18:29:17.072+00', NULL, 2, 24),
	(308, 'Q. Hoàng Mai', '2020-08-10 18:29:17.072+00', '2020-08-10 18:29:17.072+00', NULL, 2, 24),
	(309, 'Q. Long Biên', '2020-08-10 18:29:17.072+00', '2020-08-10 18:29:17.072+00', NULL, 2, 24),
	(310, 'Mê Linh', '2020-08-10 18:29:17.072+00', '2020-08-10 18:29:17.072+00', NULL, 2, 24),
	(311, 'Mỹ Đức', '2020-08-10 18:29:17.072+00', '2020-08-10 18:29:17.072+00', NULL, 2, 24),
	(312, 'Q. Nam Từ Liêm', '2020-08-10 18:29:17.072+00', '2020-08-10 18:29:17.072+00', NULL, 2, 24),
	(313, 'Phú Xuyên', '2020-08-10 18:29:17.072+00', '2020-08-10 18:29:17.072+00', NULL, 2, 24),
	(314, 'Phúc Thọ', '2020-08-10 18:29:17.072+00', '2020-08-10 18:29:17.072+00', NULL, 2, 24),
	(315, 'Quốc Oai', '2020-08-10 18:29:17.072+00', '2020-08-10 18:29:17.072+00', NULL, 2, 24),
	(316, 'Sóc Sơn', '2020-08-10 18:29:17.072+00', '2020-08-10 18:29:17.072+00', NULL, 2, 24),
	(317, 'TX. Sơn Tây', '2020-08-10 18:29:17.072+00', '2020-08-10 18:29:17.072+00', NULL, 2, 24),
	(318, 'Q. Tây Hồ', '2020-08-10 18:29:17.072+00', '2020-08-10 18:29:17.072+00', NULL, 2, 24),
	(319, 'Thạch Thất', '2020-08-10 18:29:17.072+00', '2020-08-10 18:29:17.072+00', NULL, 2, 24),
	(320, 'Thanh Oai', '2020-08-10 18:29:17.072+00', '2020-08-10 18:29:17.072+00', NULL, 2, 24),
	(321, 'Thanh Trì', '2020-08-10 18:29:17.072+00', '2020-08-10 18:29:17.072+00', NULL, 2, 24),
	(322, 'Q. Thanh Xuân', '2020-08-10 18:29:17.072+00', '2020-08-10 18:29:17.072+00', NULL, 2, 24),
	(323, 'Thường Tín', '2020-08-10 18:29:17.072+00', '2020-08-10 18:29:17.072+00', NULL, 2, 24),
	(324, 'Ứng Hòa', '2020-08-10 18:29:17.072+00', '2020-08-10 18:29:17.072+00', NULL, 2, 24),
	(325, 'Can Lộc', '2020-08-10 18:29:17.073+00', '2020-08-10 18:29:17.073+00', NULL, 2, 25),
	(328, 'TP. Hà Tĩnh', '2020-08-10 18:29:17.073+00', '2020-08-10 18:29:17.073+00', NULL, 2, 25),
	(327, 'Đức Thọ', '2020-08-10 18:29:17.073+00', '2020-08-10 18:29:17.073+00', NULL, 2, 25),
	(329, 'TX. Hồng Lĩnh', '2020-08-10 18:29:17.073+00', '2020-08-10 18:29:17.073+00', NULL, 2, 25),
	(330, 'Hương Khê', '2020-08-10 18:29:17.073+00', '2020-08-10 18:29:17.073+00', NULL, 2, 25),
	(331, 'Hương Sơn', '2020-08-10 18:29:17.073+00', '2020-08-10 18:29:17.073+00', NULL, 2, 25),
	(332, 'Kỳ Anh', '2020-08-10 18:29:17.073+00', '2020-08-10 18:29:17.073+00', NULL, 2, 25),
	(333, 'TX. Kỳ Anh', '2020-08-10 18:29:17.073+00', '2020-08-10 18:29:17.073+00', NULL, 2, 25),
	(334, 'Lộc Hà', '2020-08-10 18:29:17.073+00', '2020-08-10 18:29:17.073+00', NULL, 2, 25),
	(335, 'Nghi Xuân', '2020-08-10 18:29:17.073+00', '2020-08-10 18:29:17.073+00', NULL, 2, 25),
	(336, 'Thạch Hà', '2020-08-10 18:29:17.073+00', '2020-08-10 18:29:17.073+00', NULL, 2, 25),
	(337, 'Vũ Quang', '2020-08-10 18:29:17.073+00', '2020-08-10 18:29:17.073+00', NULL, 2, 25),
	(338, 'Bình Giang', '2020-08-10 18:29:17.073+00', '2020-08-10 18:29:17.073+00', NULL, 2, 26),
	(339, 'Cẩm Giàng', '2020-08-10 18:29:17.073+00', '2020-08-10 18:29:17.073+00', NULL, 2, 26),
	(340, 'TX. Chí Linh', '2020-08-10 18:29:17.073+00', '2020-08-10 18:29:17.073+00', NULL, 2, 26),
	(345, 'Nam Sách', '2020-08-10 18:29:17.073+00', '2020-08-10 18:29:17.073+00', NULL, 2, 26),
	(350, 'An Dương', '2020-08-10 18:29:17.074+00', '2020-08-10 18:29:17.074+00', NULL, 2, 27),
	(352, 'HĐ. Bạch Long Vĩ', '2020-08-10 18:29:17.074+00', '2020-08-10 18:29:17.074+00', NULL, 2, 27),
	(358, 'Q. Kiến An', '2020-08-10 18:29:17.074+00', '2020-08-10 18:29:17.074+00', NULL, 2, 27),
	(363, 'Tiên Lãng', '2020-08-10 18:29:17.074+00', '2020-08-10 18:29:17.074+00', NULL, 2, 27),
	(367, 'TX. Long Mỹ', '2020-08-10 18:29:17.074+00', '2020-08-10 18:29:17.074+00', NULL, 2, 28),
	(373, 'Cao Phong', '2020-08-10 18:29:17.076+00', '2020-08-10 18:29:17.076+00', NULL, 2, 29),
	(486, 'TP. Nam Định', '2020-08-10 18:29:17.082+00', '2020-08-10 18:29:17.082+00', NULL, 2, 39),
	(378, 'Lạc Sơn', '2020-08-10 18:29:17.076+00', '2020-08-10 18:29:17.076+00', NULL, 2, 29),
	(384, 'Ân Thi', '2020-08-10 18:29:17.076+00', '2020-08-10 18:29:17.076+00', NULL, 2, 30),
	(389, 'Phù Cừ', '2020-08-10 18:29:17.076+00', '2020-08-10 18:29:17.076+00', NULL, 2, 30),
	(395, 'TP. Cam Ranh', '2020-08-10 18:29:17.077+00', '2020-08-10 18:29:17.077+00', NULL, 2, 31),
	(400, 'TX. Ninh Hòa', '2020-08-10 18:29:17.077+00', '2020-08-10 18:29:17.077+00', NULL, 2, 31),
	(491, 'Xuân Trường', '2020-08-10 18:29:17.082+00', '2020-08-10 18:29:17.082+00', NULL, 2, 39),
	(498, 'TX. Hoàng Mai', '2020-08-10 18:29:17.082+00', '2020-08-10 18:29:17.082+00', NULL, 2, 40),
	(404, 'An Minh', '2020-08-10 18:29:17.077+00', '2020-08-10 18:29:17.077+00', NULL, 2, 32),
	(410, 'Hòn Đất', '2020-08-10 18:29:17.077+00', '2020-08-10 18:29:17.077+00', NULL, 2, 32),
	(414, 'TP. Rạch Giá', '2020-08-10 18:29:17.078+00', '2020-08-10 18:29:17.078+00', NULL, 2, 32),
	(418, 'Đắk Glei', '2020-08-10 18:29:17.078+00', '2020-08-10 18:29:17.078+00', NULL, 2, 33),
	(426, 'Sa Thầy', '2020-08-10 18:29:17.078+00', '2020-08-10 18:29:17.078+00', NULL, 2, 33),
	(502, 'Nghi Lộc', '2020-08-10 18:29:17.082+00', '2020-08-10 18:29:17.082+00', NULL, 2, 40),
	(431, 'Phong Thổ', '2020-08-10 18:29:17.078+00', '2020-08-10 18:29:17.078+00', NULL, 2, 34),
	(436, 'Bảo Lâm', '2020-08-10 18:29:17.079+00', '2020-08-10 18:29:17.079+00', NULL, 2, 35),
	(443, 'Đam Rông', '2020-08-10 18:29:17.079+00', '2020-08-10 18:29:17.079+00', NULL, 2, 35),
	(446, 'Lạc Dương', '2020-08-10 18:29:17.079+00', '2020-08-10 18:29:17.079+00', NULL, 2, 35),
	(455, 'Lộc Bình', '2020-08-10 18:29:17.08+00', '2020-08-10 18:29:17.08+00', NULL, 2, 36),
	(460, 'Bảo Yên', '2020-08-10 18:29:17.08+00', '2020-08-10 18:29:17.08+00', NULL, 2, 37),
	(466, 'Si Ma Cai', '2020-08-10 18:29:17.081+00', '2020-08-10 18:29:17.081+00', NULL, 2, 37),
	(474, 'TX. Kiến Tường', '2020-08-10 18:29:17.081+00', '2020-08-10 18:29:17.081+00', NULL, 2, 38),
	(482, 'Vĩnh Hưng', '2020-08-10 18:29:17.081+00', '2020-08-10 18:29:17.081+00', NULL, 2, 38),
	(508, 'Tân Kỳ', '2020-08-10 18:29:17.083+00', '2020-08-10 18:29:17.083+00', NULL, 2, 40),
	(513, 'Yên Thành', '2020-08-10 18:29:17.083+00', '2020-08-10 18:29:17.083+00', NULL, 2, 40),
	(519, 'TP. Tam Điệp', '2020-08-10 18:29:17.083+00', '2020-08-10 18:29:17.083+00', NULL, 2, 41),
	(526, 'TP. Phan Rang-Tháp Chàm', '2020-08-10 18:29:17.083+00', '2020-08-10 18:29:17.083+00', NULL, 2, 42),
	(530, 'Đoan Hùng', '2020-08-10 18:29:17.083+00', '2020-08-10 18:29:17.083+00', NULL, 2, 43),
	(535, 'Tam Nông', '2020-08-10 18:29:17.084+00', '2020-08-10 18:29:17.084+00', NULL, 2, 43),
	(540, 'TP. Việt Trì', '2020-08-10 18:29:17.084+00', '2020-08-10 18:29:17.084+00', NULL, 2, 43),
	(546, 'Sông Hinh', '2020-08-10 18:29:17.085+00', '2020-08-10 18:29:17.085+00', NULL, 2, 44),
	(551, 'TX. Ba Đồn', '2020-08-10 18:29:17.085+00', '2020-08-10 18:29:17.085+00', NULL, 2, 45),
	(555, 'Minh Hóa', '2020-08-10 18:29:17.085+00', '2020-08-10 18:29:17.085+00', NULL, 2, 45),
	(616, 'Cù Lao Dung', '2020-08-10 18:29:17.09+00', '2020-08-10 18:29:17.09+00', NULL, 2, 50),
	(559, 'Bắc Trà My', '2020-08-10 18:29:17.086+00', '2020-08-10 18:29:17.086+00', NULL, 2, 46),
	(563, 'Đông Giang', '2020-08-10 18:29:17.086+00', '2020-08-10 18:29:17.086+00', NULL, 2, 46),
	(568, 'Nông Sơn', '2020-08-10 18:29:17.086+00', '2020-08-10 18:29:17.086+00', NULL, 2, 46),
	(572, 'Quế Sơn', '2020-08-10 18:29:17.087+00', '2020-08-10 18:29:17.087+00', NULL, 2, 46),
	(577, 'Ba Tơ', '2020-08-10 18:29:17.087+00', '2020-08-10 18:29:17.087+00', NULL, 2, 47),
	(579, 'Đức Phổ', '2020-08-10 18:29:17.088+00', '2020-08-10 18:29:17.088+00', NULL, 2, 47),
	(585, 'Sơn Hà', '2020-08-10 18:29:17.088+00', '2020-08-10 18:29:17.088+00', NULL, 2, 47),
	(590, 'Tư Nghĩa', '2020-08-10 18:29:17.088+00', '2020-08-10 18:29:17.088+00', NULL, 2, 47),
	(594, 'HĐ. Cô Tô', '2020-08-10 18:29:17.089+00', '2020-08-10 18:29:17.089+00', NULL, 2, 48),
	(598, 'Hải Hà', '2020-08-10 18:29:17.089+00', '2020-08-10 18:29:17.089+00', NULL, 2, 48),
	(603, 'TP. Uông Bí', '2020-08-10 18:29:17.089+00', '2020-08-10 18:29:17.089+00', NULL, 2, 48),
	(606, 'HĐ. Cồn Cỏ', '2020-08-10 18:29:17.09+00', '2020-08-10 18:29:17.09+00', NULL, 2, 49),
	(613, 'Triệu Phong', '2020-08-10 18:29:17.09+00', '2020-08-10 18:29:17.09+00', NULL, 2, 49),
	(623, 'Thạnh Trị', '2020-08-10 18:29:17.092+00', '2020-08-10 18:29:17.092+00', NULL, 2, 50),
	(625, 'TX. Vĩnh Châu', '2020-08-10 18:29:17.092+00', '2020-08-10 18:29:17.092+00', NULL, 2, 50),
	(633, 'Sốp Cộp', '2020-08-10 18:29:17.092+00', '2020-08-10 18:29:17.092+00', NULL, 2, 51),
	(640, 'Dương Minh Châu', '2020-08-10 18:29:17.093+00', '2020-08-10 18:29:17.093+00', NULL, 2, 52),
	(646, 'Trảng Bàng', '2020-08-10 18:29:17.093+00', '2020-08-10 18:29:17.093+00', NULL, 2, 52),
	(651, 'TP. Thái Bình', '2020-08-10 18:29:17.093+00', '2020-08-10 18:29:17.093+00', NULL, 2, 53),
	(656, 'Định Hóa', '2020-08-10 18:29:17.093+00', '2020-08-10 18:29:17.093+00', NULL, 2, 54),
	(663, 'Võ Nhai', '2020-08-10 18:29:17.094+00', '2020-08-10 18:29:17.094+00', NULL, 2, 54),
	(677, 'Nông Cống', '2020-08-10 18:29:17.094+00', '2020-08-10 18:29:17.094+00', NULL, 2, 55),
	(666, 'Cẩm Thủy', '2020-08-10 18:29:17.094+00', '2020-08-10 18:29:17.094+00', NULL, 2, 55),
	(670, 'Hoằng Hóa', '2020-08-10 18:29:17.094+00', '2020-08-10 18:29:17.094+00', NULL, 2, 55),
	(674, 'Ngọc Lặc', '2020-08-10 18:29:17.094+00', '2020-08-10 18:29:17.094+00', NULL, 2, 55),
	(682, 'Thạch Thành', '2020-08-10 18:29:17.094+00', '2020-08-10 18:29:17.094+00', NULL, 2, 55),
	(687, 'Tĩnh Gia', '2020-08-10 18:29:17.095+00', '2020-08-10 18:29:17.095+00', NULL, 2, 55),
	(692, 'TX. Hương Thủy', '2020-08-10 18:29:17.095+00', '2020-08-10 18:29:17.095+00', NULL, 2, 56),
	(698, 'Phú Vang', '2020-08-10 18:29:17.096+00', '2020-08-10 18:29:17.096+00', NULL, 2, 56),
	(703, 'Châu Thành', '2020-08-10 18:29:17.097+00', '2020-08-10 18:29:17.097+00', NULL, 2, 57),
	(709, 'Tân Phú Đông', '2020-08-10 18:29:17.097+00', '2020-08-10 18:29:17.097+00', NULL, 2, 57),
	(714, 'Cần Giờ', '2020-08-10 18:29:17.097+00', '2020-08-10 18:29:17.097+00', NULL, 2, 58),
	(719, 'Q. Phú Nhuận', '2020-08-10 18:29:17.097+00', '2020-08-10 18:29:17.097+00', NULL, 2, 58),
	(723, 'Quận 4', '2020-08-10 18:29:17.097+00', '2020-08-10 18:29:17.097+00', NULL, 2, 58),
	(728, 'Quận 9', '2020-08-10 18:29:17.097+00', '2020-08-10 18:29:17.097+00', NULL, 2, 58),
	(733, 'Q. Tân Phú', '2020-08-10 18:29:17.098+00', '2020-08-10 18:29:17.098+00', NULL, 2, 58),
	(736, 'Cầu Kè', '2020-08-10 18:29:17.1+00', '2020-08-10 18:29:17.1+00', NULL, 2, 59),
	(741, 'Tiểu Cần', '2020-08-10 18:29:17.1+00', '2020-08-10 18:29:17.1+00', NULL, 2, 59),
	(746, 'Lâm Bình', '2020-08-10 18:29:17.1+00', '2020-08-10 18:29:17.1+00', NULL, 2, 60),
	(751, 'TX. Bình Minh', '2020-08-10 18:29:17.101+00', '2020-08-10 18:29:17.101+00', NULL, 2, 61),
	(757, 'TP. Vĩnh Long', '2020-08-10 18:29:17.101+00', '2020-08-10 18:29:17.101+00', NULL, 2, 61),
	(763, 'Tam Dương', '2020-08-10 18:29:17.101+00', '2020-08-10 18:29:17.101+00', NULL, 2, 62),
	(769, 'Mù Căng Chải', '2020-08-10 18:29:17.102+00', '2020-08-10 18:29:17.102+00', NULL, 2, 63),
	(775, 'TP. Yên Bái', '2020-08-10 18:29:17.102+00', '2020-08-10 18:29:17.102+00', NULL, 2, 63),
	(341, 'Gia Lộc', '2020-08-10 18:29:17.073+00', '2020-08-10 18:29:17.073+00', NULL, 2, 26),
	(346, 'Ninh Giang', '2020-08-10 18:29:17.073+00', '2020-08-10 18:29:17.073+00', NULL, 2, 26),
	(353, 'HĐ. Cát Hải', '2020-08-10 18:29:17.074+00', '2020-08-10 18:29:17.074+00', NULL, 2, 27),
	(359, 'Kiến Thụy', '2020-08-10 18:29:17.074+00', '2020-08-10 18:29:17.074+00', NULL, 2, 27),
	(365, 'Châu Thành', '2020-08-10 18:29:17.074+00', '2020-08-10 18:29:17.074+00', NULL, 2, 28),
	(370, 'Phụng Hiệp', '2020-08-10 18:29:17.074+00', '2020-08-10 18:29:17.074+00', NULL, 2, 28),
	(375, 'TP. Hoà Bình', '2020-08-10 18:29:17.076+00', '2020-08-10 18:29:17.076+00', NULL, 2, 29),
	(379, 'Lạc Thủy', '2020-08-10 18:29:17.076+00', '2020-08-10 18:29:17.076+00', NULL, 2, 29),
	(385, 'TP. Hưng Yên', '2020-08-10 18:29:17.076+00', '2020-08-10 18:29:17.076+00', NULL, 2, 30),
	(391, 'Văn Giang', '2020-08-10 18:29:17.077+00', '2020-08-10 18:29:17.077+00', NULL, 2, 30),
	(396, 'Diên Khánh', '2020-08-10 18:29:17.077+00', '2020-08-10 18:29:17.077+00', NULL, 2, 31),
	(402, 'Vạn Ninh', '2020-08-10 18:29:17.077+00', '2020-08-10 18:29:17.077+00', NULL, 2, 31),
	(413, 'HĐ. Phú Quốc', '2020-08-10 18:29:17.078+00', '2020-08-10 18:29:17.078+00', NULL, 2, 32),
	(422, 'Kon Plông', '2020-08-10 18:29:17.078+00', '2020-08-10 18:29:17.078+00', NULL, 2, 33),
	(494, 'Con Cuông', '2020-08-10 18:29:17.082+00', '2020-08-10 18:29:17.082+00', NULL, 2, 40),
	(407, 'Giồng Riềng', '2020-08-10 18:29:17.077+00', '2020-08-10 18:29:17.077+00', NULL, 2, 32),
	(412, 'Kiên Lương', '2020-08-10 18:29:17.078+00', '2020-08-10 18:29:17.078+00', NULL, 2, 32),
	(499, 'Hưng Nguyên', '2020-08-10 18:29:17.082+00', '2020-08-10 18:29:17.082+00', NULL, 2, 40),
	(430, 'Nậm Nhùn', '2020-08-10 18:29:17.078+00', '2020-08-10 18:29:17.078+00', NULL, 2, 34),
	(432, 'Sìn Hồ', '2020-08-10 18:29:17.078+00', '2020-08-10 18:29:17.078+00', NULL, 2, 34),
	(437, 'TP. Bảo Lộc', '2020-08-10 18:29:17.079+00', '2020-08-10 18:29:17.079+00', NULL, 2, 35),
	(441, 'Đạ Huoai', '2020-08-10 18:29:17.079+00', '2020-08-10 18:29:17.079+00', NULL, 2, 35),
	(445, 'Đức Trọng', '2020-08-10 18:29:17.079+00', '2020-08-10 18:29:17.079+00', NULL, 2, 35),
	(450, 'Cao Lộc', '2020-08-10 18:29:17.079+00', '2020-08-10 18:29:17.079+00', NULL, 2, 36),
	(454, 'TP. Lạng Sơn', '2020-08-10 18:29:17.08+00', '2020-08-10 18:29:17.08+00', NULL, 2, 36),
	(458, 'Văn Quan', '2020-08-10 18:29:17.08+00', '2020-08-10 18:29:17.08+00', NULL, 2, 36),
	(462, 'Bắc Hà', '2020-08-10 18:29:17.08+00', '2020-08-10 18:29:17.08+00', NULL, 2, 37),
	(467, 'Văn Bàn', '2020-08-10 18:29:17.081+00', '2020-08-10 18:29:17.081+00', NULL, 2, 37),
	(472, 'Đức Hòa', '2020-08-10 18:29:17.081+00', '2020-08-10 18:29:17.081+00', NULL, 2, 38),
	(477, 'Tân Hưng', '2020-08-10 18:29:17.081+00', '2020-08-10 18:29:17.081+00', NULL, 2, 38),
	(480, 'Thạnh Hóa', '2020-08-10 18:29:17.081+00', '2020-08-10 18:29:17.081+00', NULL, 2, 38),
	(487, 'Nam Trực', '2020-08-10 18:29:17.082+00', '2020-08-10 18:29:17.082+00', NULL, 2, 39),
	(503, 'Nghĩa Đàn', '2020-08-10 18:29:17.082+00', '2020-08-10 18:29:17.082+00', NULL, 2, 40),
	(505, 'Quỳ Châu', '2020-08-10 18:29:17.082+00', '2020-08-10 18:29:17.082+00', NULL, 2, 40),
	(510, 'Thanh Chương', '2020-08-10 18:29:17.083+00', '2020-08-10 18:29:17.083+00', NULL, 2, 40),
	(515, 'Hoa Lư', '2020-08-10 18:29:17.083+00', '2020-08-10 18:29:17.083+00', NULL, 2, 41),
	(520, 'Yên Khánh', '2020-08-10 18:29:17.083+00', '2020-08-10 18:29:17.083+00', NULL, 2, 41),
	(524, 'Ninh Phước', '2020-08-10 18:29:17.083+00', '2020-08-10 18:29:17.083+00', NULL, 2, 42),
	(528, 'Thuận Nam', '2020-08-10 18:29:17.083+00', '2020-08-10 18:29:17.083+00', NULL, 2, 42),
	(533, 'TX. Phú Thọ', '2020-08-10 18:29:17.083+00', '2020-08-10 18:29:17.083+00', NULL, 2, 43),
	(538, 'Thanh Sơn', '2020-08-10 18:29:17.084+00', '2020-08-10 18:29:17.084+00', NULL, 2, 43),
	(543, 'Đồng Xuân', '2020-08-10 18:29:17.085+00', '2020-08-10 18:29:17.085+00', NULL, 2, 44),
	(548, 'Tây Hòa', '2020-08-10 18:29:17.085+00', '2020-08-10 18:29:17.085+00', NULL, 2, 44),
	(556, 'Quảng Ninh', '2020-08-10 18:29:17.085+00', '2020-08-10 18:29:17.085+00', NULL, 2, 45),
	(561, 'Đại Lộc', '2020-08-10 18:29:17.086+00', '2020-08-10 18:29:17.086+00', NULL, 2, 46),
	(565, 'TP. Hội An', '2020-08-10 18:29:17.086+00', '2020-08-10 18:29:17.086+00', NULL, 2, 46),
	(570, 'Phú Ninh', '2020-08-10 18:29:17.086+00', '2020-08-10 18:29:17.086+00', NULL, 2, 46),
	(574, 'Tây Giang', '2020-08-10 18:29:17.087+00', '2020-08-10 18:29:17.087+00', NULL, 2, 46),
	(578, 'Bình Sơn', '2020-08-10 18:29:17.088+00', '2020-08-10 18:29:17.088+00', NULL, 2, 47),
	(583, 'Nghĩa Hành', '2020-08-10 18:29:17.088+00', '2020-08-10 18:29:17.088+00', NULL, 2, 47),
	(588, 'Tây Trà', '2020-08-10 18:29:17.088+00', '2020-08-10 18:29:17.088+00', NULL, 2, 47),
	(597, 'TP. Hạ Long', '2020-08-10 18:29:17.089+00', '2020-08-10 18:29:17.089+00', NULL, 2, 48),
	(601, 'TX. Quảng Yên', '2020-08-10 18:29:17.089+00', '2020-08-10 18:29:17.089+00', NULL, 2, 48),
	(608, 'TP. Đông Hà', '2020-08-10 18:29:17.09+00', '2020-08-10 18:29:17.09+00', NULL, 2, 49),
	(614, 'Vĩnh Linh', '2020-08-10 18:29:17.09+00', '2020-08-10 18:29:17.09+00', NULL, 2, 49),
	(619, 'Mỹ Tú', '2020-08-10 18:29:17.091+00', '2020-08-10 18:29:17.091+00', NULL, 2, 50),
	(624, 'Trần Đề', '2020-08-10 18:29:17.092+00', '2020-08-10 18:29:17.092+00', NULL, 2, 50),
	(629, 'Mường La', '2020-08-10 18:29:17.092+00', '2020-08-10 18:29:17.092+00', NULL, 2, 51),
	(636, 'Vân Hồ', '2020-08-10 18:29:17.092+00', '2020-08-10 18:29:17.092+00', NULL, 2, 51),
	(639, 'Châu Thành', '2020-08-10 18:29:17.093+00', '2020-08-10 18:29:17.093+00', NULL, 2, 52),
	(644, 'Tân Châu', '2020-08-10 18:29:17.093+00', '2020-08-10 18:29:17.093+00', NULL, 2, 52),
	(649, 'Kiến Xương', '2020-08-10 18:29:17.093+00', '2020-08-10 18:29:17.093+00', NULL, 2, 53),
	(654, 'Vũ Thư', '2020-08-10 18:29:17.093+00', '2020-08-10 18:29:17.093+00', NULL, 2, 53),
	(659, 'Phú Bình', '2020-08-10 18:29:17.093+00', '2020-08-10 18:29:17.093+00', NULL, 2, 54),
	(731, 'Quận 12', '2020-08-10 18:29:17.098+00', '2020-08-10 18:29:17.098+00', NULL, 2, 58),
	(664, 'Bá Thước', '2020-08-10 18:29:17.094+00', '2020-08-10 18:29:17.094+00', NULL, 2, 55),
	(669, 'Hậu Lộc', '2020-08-10 18:29:17.094+00', '2020-08-10 18:29:17.094+00', NULL, 2, 55),
	(678, 'Quan Hóa', '2020-08-10 18:29:17.094+00', '2020-08-10 18:29:17.094+00', NULL, 2, 55),
	(685, 'Thọ Xuân', '2020-08-10 18:29:17.094+00', '2020-08-10 18:29:17.094+00', NULL, 2, 55),
	(690, 'Yên Định', '2020-08-10 18:29:17.095+00', '2020-08-10 18:29:17.095+00', NULL, 2, 55),
	(696, 'Phong Điền', '2020-08-10 18:29:17.095+00', '2020-08-10 18:29:17.095+00', NULL, 2, 56),
	(700, 'Cai Lậy', '2020-08-10 18:29:17.096+00', '2020-08-10 18:29:17.096+00', NULL, 2, 57),
	(705, 'TX. Gò Công', '2020-08-10 18:29:17.097+00', '2020-08-10 18:29:17.097+00', NULL, 2, 57),
	(711, 'Bình Chánh', '2020-08-10 18:29:17.097+00', '2020-08-10 18:29:17.097+00', NULL, 2, 58),
	(715, 'Củ Chi', '2020-08-10 18:29:17.097+00', '2020-08-10 18:29:17.097+00', NULL, 2, 58),
	(721, 'Quận 2', '2020-08-10 18:29:17.097+00', '2020-08-10 18:29:17.097+00', NULL, 2, 58),
	(726, 'Quận 7', '2020-08-10 18:29:17.097+00', '2020-08-10 18:29:17.097+00', NULL, 2, 58),
	(735, 'Càng Long', '2020-08-10 18:29:17.1+00', '2020-08-10 18:29:17.1+00', NULL, 2, 59),
	(743, 'TP. Trà Vinh', '2020-08-10 18:29:17.1+00', '2020-08-10 18:29:17.1+00', NULL, 2, 59),
	(748, 'Sơn Dương', '2020-08-10 18:29:17.1+00', '2020-08-10 18:29:17.1+00', NULL, 2, 60),
	(753, 'Long Hồ', '2020-08-10 18:29:17.101+00', '2020-08-10 18:29:17.101+00', NULL, 2, 61),
	(756, 'Trà Ôn', '2020-08-10 18:29:17.101+00', '2020-08-10 18:29:17.101+00', NULL, 2, 61),
	(761, 'TX. Phúc Yên', '2020-08-10 18:29:17.101+00', '2020-08-10 18:29:17.101+00', NULL, 2, 62),
	(767, 'Yên Lạc', '2020-08-10 18:29:17.101+00', '2020-08-10 18:29:17.101+00', NULL, 2, 62);
INSERT INTO public.locations VALUES
	(773, 'Văn Chấn', '2020-08-10 18:29:17.102+00', '2020-08-10 18:29:17.102+00', NULL, 2, 63),
	(342, 'TP. Hải Dương', '2020-08-10 18:29:17.073+00', '2020-08-10 18:29:17.073+00', NULL, 2, 26),
	(347, 'Thanh Hà', '2020-08-10 18:29:17.073+00', '2020-08-10 18:29:17.073+00', NULL, 2, 26),
	(405, 'Châu Thành', '2020-08-10 18:29:17.077+00', '2020-08-10 18:29:17.077+00', NULL, 2, 32),
	(351, 'An Lão', '2020-08-10 18:29:17.074+00', '2020-08-10 18:29:17.074+00', NULL, 2, 27),
	(356, 'Q. Hải An', '2020-08-10 18:29:17.074+00', '2020-08-10 18:29:17.074+00', NULL, 2, 27),
	(361, 'Q. Ngô Quyền', '2020-08-10 18:29:17.074+00', '2020-08-10 18:29:17.074+00', NULL, 2, 27),
	(368, 'Long Mỹ', '2020-08-10 18:29:17.074+00', '2020-08-10 18:29:17.074+00', NULL, 2, 28),
	(372, 'Vị Thủy', '2020-08-10 18:29:17.075+00', '2020-08-10 18:29:17.075+00', NULL, 2, 28),
	(411, 'HĐ. Kiên Hải', '2020-08-10 18:29:17.078+00', '2020-08-10 18:29:17.078+00', NULL, 2, 32),
	(377, 'Kỳ Sơn', '2020-08-10 18:29:17.076+00', '2020-08-10 18:29:17.076+00', NULL, 2, 29),
	(382, 'Tân Lạc', '2020-08-10 18:29:17.076+00', '2020-08-10 18:29:17.076+00', NULL, 2, 29),
	(387, 'Kim Động', '2020-08-10 18:29:17.076+00', '2020-08-10 18:29:17.076+00', NULL, 2, 30),
	(392, 'Văn Lâm', '2020-08-10 18:29:17.077+00', '2020-08-10 18:29:17.077+00', NULL, 2, 30),
	(398, 'Khánh Vĩnh', '2020-08-10 18:29:17.077+00', '2020-08-10 18:29:17.077+00', NULL, 2, 31),
	(476, 'TP. Tân An', '2020-08-10 18:29:17.081+00', '2020-08-10 18:29:17.081+00', NULL, 2, 38),
	(479, 'Tân Trụ', '2020-08-10 18:29:17.081+00', '2020-08-10 18:29:17.081+00', NULL, 2, 38),
	(484, 'Hải Hậu', '2020-08-10 18:29:17.081+00', '2020-08-10 18:29:17.081+00', NULL, 2, 39),
	(415, 'Tân Hiệp', '2020-08-10 18:29:17.078+00', '2020-08-10 18:29:17.078+00', NULL, 2, 32),
	(419, 'Đắk Hà', '2020-08-10 18:29:17.078+00', '2020-08-10 18:29:17.078+00', NULL, 2, 33),
	(423, 'Kon Rẫy', '2020-08-10 18:29:17.078+00', '2020-08-10 18:29:17.078+00', NULL, 2, 33),
	(489, 'Trực Ninh', '2020-08-10 18:29:17.082+00', '2020-08-10 18:29:17.082+00', NULL, 2, 39),
	(429, 'Mường Tè', '2020-08-10 18:29:17.078+00', '2020-08-10 18:29:17.078+00', NULL, 2, 34),
	(435, 'Than Uyên', '2020-08-10 18:29:17.079+00', '2020-08-10 18:29:17.079+00', NULL, 2, 34),
	(439, 'Di Linh', '2020-08-10 18:29:17.079+00', '2020-08-10 18:29:17.079+00', NULL, 2, 35),
	(444, 'Đơn Dương', '2020-08-10 18:29:17.079+00', '2020-08-10 18:29:17.079+00', NULL, 2, 35),
	(449, 'Bình Gia', '2020-08-10 18:29:17.079+00', '2020-08-10 18:29:17.079+00', NULL, 2, 36),
	(453, 'Hữu Lũng', '2020-08-10 18:29:17.079+00', '2020-08-10 18:29:17.079+00', NULL, 2, 36),
	(459, 'Bảo Thắng', '2020-08-10 18:29:17.08+00', '2020-08-10 18:29:17.08+00', NULL, 2, 37),
	(463, 'TP. Lào Cai', '2020-08-10 18:29:17.081+00', '2020-08-10 18:29:17.081+00', NULL, 2, 37),
	(469, 'Cần Đước', '2020-08-10 18:29:17.081+00', '2020-08-10 18:29:17.081+00', NULL, 2, 38),
	(471, 'Châu Thành', '2020-08-10 18:29:17.081+00', '2020-08-10 18:29:17.081+00', NULL, 2, 38),
	(492, 'Ý Yên', '2020-08-10 18:29:17.082+00', '2020-08-10 18:29:17.082+00', NULL, 2, 39),
	(497, 'Đô Lương', '2020-08-10 18:29:17.082+00', '2020-08-10 18:29:17.082+00', NULL, 2, 40),
	(501, 'Nam Đàn', '2020-08-10 18:29:17.082+00', '2020-08-10 18:29:17.082+00', NULL, 2, 40),
	(506, 'Quỳ Hợp', '2020-08-10 18:29:17.082+00', '2020-08-10 18:29:17.082+00', NULL, 2, 40),
	(511, 'Tương Dương', '2020-08-10 18:29:17.083+00', '2020-08-10 18:29:17.083+00', NULL, 2, 40),
	(516, 'Kim Sơn', '2020-08-10 18:29:17.083+00', '2020-08-10 18:29:17.083+00', NULL, 2, 41),
	(523, 'Ninh Hải', '2020-08-10 18:29:17.083+00', '2020-08-10 18:29:17.083+00', NULL, 2, 42),
	(531, 'Hạ Hòa', '2020-08-10 18:29:17.083+00', '2020-08-10 18:29:17.083+00', NULL, 2, 43),
	(536, 'Tân Sơn', '2020-08-10 18:29:17.084+00', '2020-08-10 18:29:17.084+00', NULL, 2, 43),
	(539, 'Thanh Thủy', '2020-08-10 18:29:17.084+00', '2020-08-10 18:29:17.084+00', NULL, 2, 43),
	(545, 'TX. Sông Cầu', '2020-08-10 18:29:17.085+00', '2020-08-10 18:29:17.085+00', NULL, 2, 44),
	(550, 'TP. Tuy Hòa', '2020-08-10 18:29:17.085+00', '2020-08-10 18:29:17.085+00', NULL, 2, 44),
	(554, 'Lệ Thủy', '2020-08-10 18:29:17.085+00', '2020-08-10 18:29:17.085+00', NULL, 2, 45),
	(558, 'Tuyên Hóa', '2020-08-10 18:29:17.086+00', '2020-08-10 18:29:17.086+00', NULL, 2, 45),
	(564, 'Hiệp Đức', '2020-08-10 18:29:17.086+00', '2020-08-10 18:29:17.086+00', NULL, 2, 46),
	(569, 'Núi Thành', '2020-08-10 18:29:17.086+00', '2020-08-10 18:29:17.086+00', NULL, 2, 46),
	(575, 'Thăng Bình', '2020-08-10 18:29:17.087+00', '2020-08-10 18:29:17.087+00', NULL, 2, 46),
	(580, 'HĐ. Lý Sơn', '2020-08-10 18:29:17.088+00', '2020-08-10 18:29:17.088+00', NULL, 2, 47),
	(586, 'Sơn Tây', '2020-08-10 18:29:17.088+00', '2020-08-10 18:29:17.088+00', NULL, 2, 47),
	(591, 'Ba Chẽ', '2020-08-10 18:29:17.089+00', '2020-08-10 18:29:17.089+00', NULL, 2, 48),
	(595, 'Đầm Hà', '2020-08-10 18:29:17.089+00', '2020-08-10 18:29:17.089+00', NULL, 2, 48),
	(602, 'Tiên Yên', '2020-08-10 18:29:17.089+00', '2020-08-10 18:29:17.089+00', NULL, 2, 48),
	(607, 'Đa Krông', '2020-08-10 18:29:17.09+00', '2020-08-10 18:29:17.09+00', NULL, 2, 49),
	(612, 'TX. Quảng Trị', '2020-08-10 18:29:17.09+00', '2020-08-10 18:29:17.09+00', NULL, 2, 49),
	(617, 'Kế Sách', '2020-08-10 18:29:17.09+00', '2020-08-10 18:29:17.09+00', NULL, 2, 50),
	(620, 'Mỹ Xuyên', '2020-08-10 18:29:17.091+00', '2020-08-10 18:29:17.091+00', NULL, 2, 50),
	(628, 'Mộc Châu', '2020-08-10 18:29:17.092+00', '2020-08-10 18:29:17.092+00', NULL, 2, 51),
	(632, 'Sông Mã', '2020-08-10 18:29:17.092+00', '2020-08-10 18:29:17.092+00', NULL, 2, 51),
	(637, 'Yên Châu', '2020-08-10 18:29:17.092+00', '2020-08-10 18:29:17.092+00', NULL, 2, 51),
	(642, 'Hòa Thành', '2020-08-10 18:29:17.093+00', '2020-08-10 18:29:17.093+00', NULL, 2, 52),
	(647, 'Đông Hưng', '2020-08-10 18:29:17.093+00', '2020-08-10 18:29:17.093+00', NULL, 2, 53),
	(653, 'Tiền Hải', '2020-08-10 18:29:17.093+00', '2020-08-10 18:29:17.093+00', NULL, 2, 53),
	(655, 'Đại Từ', '2020-08-10 18:29:17.093+00', '2020-08-10 18:29:17.093+00', NULL, 2, 54),
	(661, 'TP. Sông Công', '2020-08-10 18:29:17.093+00', '2020-08-10 18:29:17.093+00', NULL, 2, 54),
	(665, 'TX. Bỉm Sơn', '2020-08-10 18:29:17.094+00', '2020-08-10 18:29:17.094+00', NULL, 2, 55),
	(673, 'Nga Sơn', '2020-08-10 18:29:17.094+00', '2020-08-10 18:29:17.094+00', NULL, 2, 55),
	(679, 'Quan Sơn', '2020-08-10 18:29:17.094+00', '2020-08-10 18:29:17.094+00', NULL, 2, 55),
	(686, 'Thường Xuân', '2020-08-10 18:29:17.094+00', '2020-08-10 18:29:17.094+00', NULL, 2, 55),
	(691, 'TP. Huế', '2020-08-10 18:29:17.095+00', '2020-08-10 18:29:17.095+00', NULL, 2, 56),
	(697, 'Phú Lộc', '2020-08-10 18:29:17.095+00', '2020-08-10 18:29:17.095+00', NULL, 2, 56),
	(702, 'Cái Bè', '2020-08-10 18:29:17.097+00', '2020-08-10 18:29:17.097+00', NULL, 2, 57),
	(708, 'TP. Mỹ Tho', '2020-08-10 18:29:17.097+00', '2020-08-10 18:29:17.097+00', NULL, 2, 57),
	(713, 'Q. Bình Thạnh', '2020-08-10 18:29:17.097+00', '2020-08-10 18:29:17.097+00', NULL, 2, 58),
	(717, 'Hóc Môn', '2020-08-10 18:29:17.097+00', '2020-08-10 18:29:17.097+00', NULL, 2, 58),
	(722, 'Quận 3', '2020-08-10 18:29:17.097+00', '2020-08-10 18:29:17.097+00', NULL, 2, 58),
	(727, 'Quận 8', '2020-08-10 18:29:17.097+00', '2020-08-10 18:29:17.097+00', NULL, 2, 58),
	(732, 'Q. Tân Bình', '2020-08-10 18:29:17.098+00', '2020-08-10 18:29:17.098+00', NULL, 2, 58),
	(738, 'Châu Thành', '2020-08-10 18:29:17.1+00', '2020-08-10 18:29:17.1+00', NULL, 2, 59),
	(742, 'Trà Cú', '2020-08-10 18:29:17.1+00', '2020-08-10 18:29:17.1+00', NULL, 2, 59),
	(747, 'Na Hang', '2020-08-10 18:29:17.1+00', '2020-08-10 18:29:17.1+00', NULL, 2, 60),
	(766, 'TP. Vĩnh Yên', '2020-08-10 18:29:17.101+00', '2020-08-10 18:29:17.101+00', NULL, 2, 62),
	(752, 'Bình Tân', '2020-08-10 18:29:17.101+00', '2020-08-10 18:29:17.101+00', NULL, 2, 61),
	(758, 'Vũng Liêm', '2020-08-10 18:29:17.101+00', '2020-08-10 18:29:17.101+00', NULL, 2, 61),
	(762, 'Sông Lô', '2020-08-10 18:29:17.101+00', '2020-08-10 18:29:17.101+00', NULL, 2, 62),
	(771, 'Trạm Tấu', '2020-08-10 18:29:17.102+00', '2020-08-10 18:29:17.102+00', NULL, 2, 63),
	(774, 'Văn Yên', '2020-08-10 18:29:17.102+00', '2020-08-10 18:29:17.102+00', NULL, 2, 63),
	(343, 'Kim Thành', '2020-08-10 18:29:17.073+00', '2020-08-10 18:29:17.073+00', NULL, 2, 26),
	(348, 'Thanh Miện', '2020-08-10 18:29:17.074+00', '2020-08-10 18:29:17.074+00', NULL, 2, 26),
	(354, 'Q. Dương Kinh', '2020-08-10 18:29:17.074+00', '2020-08-10 18:29:17.074+00', NULL, 2, 27),
	(360, 'Q. Lê Chân', '2020-08-10 18:29:17.074+00', '2020-08-10 18:29:17.074+00', NULL, 2, 27),
	(366, 'Châu Thành A', '2020-08-10 18:29:17.074+00', '2020-08-10 18:29:17.074+00', NULL, 2, 28),
	(374, 'Đà Bắc', '2020-08-10 18:29:17.076+00', '2020-08-10 18:29:17.076+00', NULL, 2, 29),
	(380, 'Lương Sơn', '2020-08-10 18:29:17.076+00', '2020-08-10 18:29:17.076+00', NULL, 2, 29),
	(386, 'Khoái Châu', '2020-08-10 18:29:17.076+00', '2020-08-10 18:29:17.076+00', NULL, 2, 30),
	(390, 'Tiên Lữ', '2020-08-10 18:29:17.077+00', '2020-08-10 18:29:17.077+00', NULL, 2, 30),
	(394, 'Cam Lâm', '2020-08-10 18:29:17.077+00', '2020-08-10 18:29:17.077+00', NULL, 2, 31),
	(399, 'TP. Nha Trang', '2020-08-10 18:29:17.077+00', '2020-08-10 18:29:17.077+00', NULL, 2, 31),
	(401, 'HĐ. Trường Sa', '2020-08-10 18:29:17.077+00', '2020-08-10 18:29:17.077+00', NULL, 2, 31),
	(473, 'Đức Huệ', '2020-08-10 18:29:17.081+00', '2020-08-10 18:29:17.081+00', NULL, 2, 38),
	(478, 'Tân Thạnh', '2020-08-10 18:29:17.081+00', '2020-08-10 18:29:17.081+00', NULL, 2, 38),
	(481, 'Thủ Thừa', '2020-08-10 18:29:17.081+00', '2020-08-10 18:29:17.081+00', NULL, 2, 38),
	(406, 'Giang Thành', '2020-08-10 18:29:17.077+00', '2020-08-10 18:29:17.077+00', NULL, 2, 32),
	(408, 'Gò Quao', '2020-08-10 18:29:17.077+00', '2020-08-10 18:29:17.077+00', NULL, 2, 32),
	(417, 'Vĩnh Thuận', '2020-08-10 18:29:17.078+00', '2020-08-10 18:29:17.078+00', NULL, 2, 32),
	(421, 'Ia H''Drai', '2020-08-10 18:29:17.078+00', '2020-08-10 18:29:17.078+00', NULL, 2, 33),
	(425, 'Ngọc Hồi', '2020-08-10 18:29:17.078+00', '2020-08-10 18:29:17.078+00', NULL, 2, 33),
	(427, 'Tu Mơ Rông', '2020-08-10 18:29:17.078+00', '2020-08-10 18:29:17.078+00', NULL, 2, 33),
	(434, 'Tân Uyên', '2020-08-10 18:29:17.079+00', '2020-08-10 18:29:17.079+00', NULL, 2, 34),
	(440, 'TP. Đà Lạt', '2020-08-10 18:29:17.079+00', '2020-08-10 18:29:17.079+00', NULL, 2, 35),
	(448, 'Bắc Sơn', '2020-08-10 18:29:17.079+00', '2020-08-10 18:29:17.079+00', NULL, 2, 36),
	(452, 'Đình Lập', '2020-08-10 18:29:17.079+00', '2020-08-10 18:29:17.079+00', NULL, 2, 36),
	(457, 'Vãn Lãng', '2020-08-10 18:29:17.08+00', '2020-08-10 18:29:17.08+00', NULL, 2, 36),
	(464, 'Mường Khương', '2020-08-10 18:29:17.081+00', '2020-08-10 18:29:17.081+00', NULL, 2, 37),
	(468, 'Bến Lức', '2020-08-10 18:29:17.081+00', '2020-08-10 18:29:17.081+00', NULL, 2, 38),
	(488, 'Nghĩa Hưng', '2020-08-10 18:29:17.082+00', '2020-08-10 18:29:17.082+00', NULL, 2, 39),
	(493, 'Anh Sơn', '2020-08-10 18:29:17.082+00', '2020-08-10 18:29:17.082+00', NULL, 2, 40),
	(496, 'Diễn Châu', '2020-08-10 18:29:17.082+00', '2020-08-10 18:29:17.082+00', NULL, 2, 40),
	(500, 'Kỳ Sơn', '2020-08-10 18:29:17.082+00', '2020-08-10 18:29:17.082+00', NULL, 2, 40),
	(507, 'Quỳnh Lưu', '2020-08-10 18:29:17.082+00', '2020-08-10 18:29:17.082+00', NULL, 2, 40),
	(512, 'TP. Vinh', '2020-08-10 18:29:17.083+00', '2020-08-10 18:29:17.083+00', NULL, 2, 40),
	(518, 'TP. Ninh Bình', '2020-08-10 18:29:17.083+00', '2020-08-10 18:29:17.083+00', NULL, 2, 41),
	(522, 'Bác Ái', '2020-08-10 18:29:17.083+00', '2020-08-10 18:29:17.083+00', NULL, 2, 42),
	(527, 'Thuận Bắc', '2020-08-10 18:29:17.083+00', '2020-08-10 18:29:17.083+00', NULL, 2, 42),
	(532, 'Lâm Thao', '2020-08-10 18:29:17.083+00', '2020-08-10 18:29:17.083+00', NULL, 2, 43),
	(537, 'Thanh Ba', '2020-08-10 18:29:17.084+00', '2020-08-10 18:29:17.084+00', NULL, 2, 43),
	(542, 'Đông Hòa', '2020-08-10 18:29:17.085+00', '2020-08-10 18:29:17.085+00', NULL, 2, 44),
	(547, 'Sơn Hòa', '2020-08-10 18:29:17.085+00', '2020-08-10 18:29:17.085+00', NULL, 2, 44),
	(552, 'Bố Trạch', '2020-08-10 18:29:17.085+00', '2020-08-10 18:29:17.085+00', NULL, 2, 45),
	(562, 'TX. Điện Bàn', '2020-08-10 18:29:17.086+00', '2020-08-10 18:29:17.086+00', NULL, 2, 46),
	(567, 'Nam Trà My', '2020-08-10 18:29:17.086+00', '2020-08-10 18:29:17.086+00', NULL, 2, 46),
	(573, 'TP. Tam Kỳ', '2020-08-10 18:29:17.087+00', '2020-08-10 18:29:17.087+00', NULL, 2, 46),
	(582, 'Mộ Đức', '2020-08-10 18:29:17.088+00', '2020-08-10 18:29:17.088+00', NULL, 2, 47),
	(584, 'TP. Quảng Ngãi', '2020-08-10 18:29:17.088+00', '2020-08-10 18:29:17.088+00', NULL, 2, 47),
	(589, 'Trà Bồng', '2020-08-10 18:29:17.088+00', '2020-08-10 18:29:17.088+00', NULL, 2, 47),
	(593, 'TP. Cẩm Phả', '2020-08-10 18:29:17.089+00', '2020-08-10 18:29:17.089+00', NULL, 2, 48),
	(599, 'Hoành Bồ', '2020-08-10 18:29:17.089+00', '2020-08-10 18:29:17.089+00', NULL, 2, 48),
	(604, 'HĐ. Vân Đồn', '2020-08-10 18:29:17.089+00', '2020-08-10 18:29:17.089+00', NULL, 2, 48),
	(609, 'Gio Linh', '2020-08-10 18:29:17.09+00', '2020-08-10 18:29:17.09+00', NULL, 2, 49),
	(611, 'Hướng Hóa', '2020-08-10 18:29:17.09+00', '2020-08-10 18:29:17.09+00', NULL, 2, 49),
	(615, 'Châu Thành', '2020-08-10 18:29:17.09+00', '2020-08-10 18:29:17.09+00', NULL, 2, 49),
	(622, 'TP. Sóc Trăng', '2020-08-10 18:29:17.092+00', '2020-08-10 18:29:17.092+00', NULL, 2, 50),
	(660, 'Phú Lương', '2020-08-10 18:29:17.093+00', '2020-08-10 18:29:17.093+00', NULL, 2, 54),
	(627, 'Mai Sơn', '2020-08-10 18:29:17.092+00', '2020-08-10 18:29:17.092+00', NULL, 2, 51),
	(631, 'Quỳnh Nhai', '2020-08-10 18:29:17.092+00', '2020-08-10 18:29:17.092+00', NULL, 2, 51),
	(635, 'Thuận Châu', '2020-08-10 18:29:17.092+00', '2020-08-10 18:29:17.092+00', NULL, 2, 51),
	(641, 'Gò Dầu', '2020-08-10 18:29:17.093+00', '2020-08-10 18:29:17.093+00', NULL, 2, 52),
	(645, 'TP. Tây Ninh', '2020-08-10 18:29:17.093+00', '2020-08-10 18:29:17.093+00', NULL, 2, 52),
	(650, 'Quỳnh Phụ', '2020-08-10 18:29:17.093+00', '2020-08-10 18:29:17.093+00', NULL, 2, 53),
	(657, 'Đồng Hỷ', '2020-08-10 18:29:17.093+00', '2020-08-10 18:29:17.093+00', NULL, 2, 54),
	(668, 'Hà Trung', '2020-08-10 18:29:17.094+00', '2020-08-10 18:29:17.094+00', NULL, 2, 55),
	(672, 'Mường Lát', '2020-08-10 18:29:17.094+00', '2020-08-10 18:29:17.094+00', NULL, 2, 55),
	(675, 'Như Thanh', '2020-08-10 18:29:17.094+00', '2020-08-10 18:29:17.094+00', NULL, 2, 55),
	(681, 'TX. Sầm Sơn', '2020-08-10 18:29:17.094+00', '2020-08-10 18:29:17.094+00', NULL, 2, 55),
	(683, 'TP. Thanh Hóa', '2020-08-10 18:29:17.094+00', '2020-08-10 18:29:17.094+00', NULL, 2, 55),
	(688, 'Triệu Sơn', '2020-08-10 18:29:17.095+00', '2020-08-10 18:29:17.095+00', NULL, 2, 55),
	(693, 'TX. Hương Trà', '2020-08-10 18:29:17.095+00', '2020-08-10 18:29:17.095+00', NULL, 2, 56),
	(695, 'A Lưới', '2020-08-10 18:29:17.095+00', '2020-08-10 18:29:17.095+00', NULL, 2, 56),
	(701, 'TX. Cai Lậy', '2020-08-10 18:29:17.096+00', '2020-08-10 18:29:17.096+00', NULL, 2, 57),
	(706, 'Gò Công Đông', '2020-08-10 18:29:17.097+00', '2020-08-10 18:29:17.097+00', NULL, 2, 57),
	(710, 'Tân Phước', '2020-08-10 18:29:17.097+00', '2020-08-10 18:29:17.097+00', NULL, 2, 57),
	(716, 'Q. Gò Vấp', '2020-08-10 18:29:17.097+00', '2020-08-10 18:29:17.097+00', NULL, 2, 58),
	(720, 'Quận 1', '2020-08-10 18:29:17.097+00', '2020-08-10 18:29:17.097+00', NULL, 2, 58),
	(725, 'Quận 6', '2020-08-10 18:29:17.097+00', '2020-08-10 18:29:17.097+00', NULL, 2, 58),
	(729, 'Quận 10', '2020-08-10 18:29:17.098+00', '2020-08-10 18:29:17.098+00', NULL, 2, 58),
	(734, 'Q. Thủ Đức', '2020-08-10 18:29:17.1+00', '2020-08-10 18:29:17.1+00', NULL, 2, 58),
	(739, 'TX. Duyên Hải', '2020-08-10 18:29:17.1+00', '2020-08-10 18:29:17.1+00', NULL, 2, 59),
	(744, 'Chiêm Hóa', '2020-08-10 18:29:17.1+00', '2020-08-10 18:29:17.1+00', NULL, 2, 60),
	(750, 'Yên Sơn', '2020-08-10 18:29:17.101+00', '2020-08-10 18:29:17.101+00', NULL, 2, 60),
	(755, 'Tam Bình', '2020-08-10 18:29:17.101+00', '2020-08-10 18:29:17.101+00', NULL, 2, 61),
	(760, 'Lập Thạch', '2020-08-10 18:29:17.101+00', '2020-08-10 18:29:17.101+00', NULL, 2, 62),
	(765, 'Vĩnh Tường', '2020-08-10 18:29:17.101+00', '2020-08-10 18:29:17.101+00', NULL, 2, 62),
	(770, 'TX. Nghĩa Lộ', '2020-08-10 18:29:17.102+00', '2020-08-10 18:29:17.102+00', NULL, 2, 63),
	(776, 'Yên Bình', '2020-08-10 18:29:17.102+00', '2020-08-10 18:29:17.102+00', NULL, 2, 63),
	(228, 'Điện Biên Đông', '2020-08-10 18:29:17.066+00', '2020-08-10 18:29:17.066+00', NULL, 2, 18),
	(230, 'TP. Điện Biên Phủ', '2020-08-10 18:29:17.066+00', '2020-08-10 18:29:17.066+00', NULL, 2, 18),
	(252, 'Hồng Ngự', '2020-08-10 18:29:17.068+00', '2020-08-10 18:29:17.068+00', NULL, 2, 20),
	(273, 'Kông Chro', '2020-08-10 18:29:17.069+00', '2020-08-10 18:29:17.069+00', NULL, 2, 21),
	(290, 'Duy Tiên', '2020-08-10 18:29:17.071+00', '2020-08-10 18:29:17.071+00', NULL, 2, 23),
	(305, 'Q. Hai Bà Trưng', '2020-08-10 18:29:17.072+00', '2020-08-10 18:29:17.072+00', NULL, 2, 24),
	(326, 'Cẩm Xuyên', '2020-08-10 18:29:17.073+00', '2020-08-10 18:29:17.073+00', NULL, 2, 25),
	(344, 'Kinh Môn', '2020-08-10 18:29:17.073+00', '2020-08-10 18:29:17.073+00', NULL, 2, 26),
	(349, 'Tứ Kỳ', '2020-08-10 18:29:17.074+00', '2020-08-10 18:29:17.074+00', NULL, 2, 26),
	(355, 'Q. Đồ Sơn', '2020-08-10 18:29:17.074+00', '2020-08-10 18:29:17.074+00', NULL, 2, 27),
	(357, 'Q. Hồng Bàng', '2020-08-10 18:29:17.074+00', '2020-08-10 18:29:17.074+00', NULL, 2, 27),
	(362, 'Thuỷ Nguyên', '2020-08-10 18:29:17.074+00', '2020-08-10 18:29:17.074+00', NULL, 2, 27),
	(364, 'Vĩnh Bảo', '2020-08-10 18:29:17.074+00', '2020-08-10 18:29:17.074+00', NULL, 2, 27),
	(369, 'TX. Ngã Bảy', '2020-08-10 18:29:17.074+00', '2020-08-10 18:29:17.074+00', NULL, 2, 28),
	(371, 'TP. Vị Thanh', '2020-08-10 18:29:17.075+00', '2020-08-10 18:29:17.075+00', NULL, 2, 28),
	(376, 'Kim Bôi', '2020-08-10 18:29:17.076+00', '2020-08-10 18:29:17.076+00', NULL, 2, 29),
	(381, 'Mai Châu', '2020-08-10 18:29:17.076+00', '2020-08-10 18:29:17.076+00', NULL, 2, 29),
	(383, 'Yên Thủy', '2020-08-10 18:29:17.076+00', '2020-08-10 18:29:17.076+00', NULL, 2, 29),
	(388, 'Mỹ Hào', '2020-08-10 18:29:17.076+00', '2020-08-10 18:29:17.076+00', NULL, 2, 30),
	(393, 'Yên Mỹ', '2020-08-10 18:29:17.077+00', '2020-08-10 18:29:17.077+00', NULL, 2, 30),
	(397, 'Khánh Sơn', '2020-08-10 18:29:17.077+00', '2020-08-10 18:29:17.077+00', NULL, 2, 31),
	(409, 'TX. Hà Tiên', '2020-08-10 18:29:17.077+00', '2020-08-10 18:29:17.077+00', NULL, 2, 32),
	(442, 'Đạ Tẻh', '2020-08-10 18:29:17.079+00', '2020-08-10 18:29:17.079+00', NULL, 2, 35),
	(428, 'TP. Lai Châu', '2020-08-10 18:29:17.078+00', '2020-08-10 18:29:17.078+00', NULL, 2, 34),
	(403, 'An Biên', '2020-08-10 18:29:17.077+00', '2020-08-10 18:29:17.077+00', NULL, 2, 32),
	(416, 'U Minh Thượng', '2020-08-10 18:29:17.078+00', '2020-08-10 18:29:17.078+00', NULL, 2, 32),
	(420, 'Đăk Tô', '2020-08-10 18:29:17.078+00', '2020-08-10 18:29:17.078+00', NULL, 2, 33),
	(424, 'TP. Kon Tum', '2020-08-10 18:29:17.078+00', '2020-08-10 18:29:17.078+00', NULL, 2, 33),
	(433, 'Tam Đường', '2020-08-10 18:29:17.078+00', '2020-08-10 18:29:17.078+00', NULL, 2, 34),
	(438, 'Cát Tiên', '2020-08-10 18:29:17.079+00', '2020-08-10 18:29:17.079+00', NULL, 2, 35),
	(447, 'Lâm Hà', '2020-08-10 18:29:17.079+00', '2020-08-10 18:29:17.079+00', NULL, 2, 35),
	(451, 'Chi Lăng', '2020-08-10 18:29:17.079+00', '2020-08-10 18:29:17.079+00', NULL, 2, 36),
	(456, 'Tràng Định', '2020-08-10 18:29:17.08+00', '2020-08-10 18:29:17.08+00', NULL, 2, 36),
	(461, 'Bát Xát', '2020-08-10 18:29:17.08+00', '2020-08-10 18:29:17.08+00', NULL, 2, 37),
	(465, 'Sa Pa', '2020-08-10 18:29:17.081+00', '2020-08-10 18:29:17.081+00', NULL, 2, 37),
	(470, 'Cần Giuộc', '2020-08-10 18:29:17.081+00', '2020-08-10 18:29:17.081+00', NULL, 2, 38),
	(475, 'Mộc Hóa', '2020-08-10 18:29:17.081+00', '2020-08-10 18:29:17.081+00', NULL, 2, 38),
	(483, 'Giao Thủy', '2020-08-10 18:29:17.081+00', '2020-08-10 18:29:17.081+00', NULL, 2, 39),
	(485, 'Mỹ Lộc', '2020-08-10 18:29:17.081+00', '2020-08-10 18:29:17.081+00', NULL, 2, 39),
	(490, 'Vụ Bản', '2020-08-10 18:29:17.082+00', '2020-08-10 18:29:17.082+00', NULL, 2, 39),
	(495, 'TX. Cửa Lò', '2020-08-10 18:29:17.082+00', '2020-08-10 18:29:17.082+00', NULL, 2, 40),
	(504, 'Quế Phong', '2020-08-10 18:29:17.082+00', '2020-08-10 18:29:17.082+00', NULL, 2, 40),
	(509, 'TX. Thái Hòa', '2020-08-10 18:29:17.083+00', '2020-08-10 18:29:17.083+00', NULL, 2, 40),
	(514, 'Gia Viễn', '2020-08-10 18:29:17.083+00', '2020-08-10 18:29:17.083+00', NULL, 2, 41),
	(517, 'Nho Quan', '2020-08-10 18:29:17.083+00', '2020-08-10 18:29:17.083+00', NULL, 2, 41),
	(521, 'Yên Mô', '2020-08-10 18:29:17.083+00', '2020-08-10 18:29:17.083+00', NULL, 2, 41),
	(525, 'Ninh Sơn', '2020-08-10 18:29:17.083+00', '2020-08-10 18:29:17.083+00', NULL, 2, 42),
	(529, 'Cẩm Khê', '2020-08-10 18:29:17.083+00', '2020-08-10 18:29:17.083+00', NULL, 2, 43),
	(534, 'Phù Ninh', '2020-08-10 18:29:17.084+00', '2020-08-10 18:29:17.084+00', NULL, 2, 43),
	(541, 'Yên Lập', '2020-08-10 18:29:17.084+00', '2020-08-10 18:29:17.084+00', NULL, 2, 43),
	(544, 'Phú Hòa', '2020-08-10 18:29:17.085+00', '2020-08-10 18:29:17.085+00', NULL, 2, 44),
	(549, 'Tuy An', '2020-08-10 18:29:17.085+00', '2020-08-10 18:29:17.085+00', NULL, 2, 44),
	(553, 'TP. Đồng Hới', '2020-08-10 18:29:17.085+00', '2020-08-10 18:29:17.085+00', NULL, 2, 45),
	(557, 'Quảng Trạch', '2020-08-10 18:29:17.085+00', '2020-08-10 18:29:17.085+00', NULL, 2, 45),
	(560, 'Duy Xuyên', '2020-08-10 18:29:17.086+00', '2020-08-10 18:29:17.086+00', NULL, 2, 46),
	(566, 'Nam Giang', '2020-08-10 18:29:17.086+00', '2020-08-10 18:29:17.086+00', NULL, 2, 46),
	(571, 'Phước Sơn', '2020-08-10 18:29:17.087+00', '2020-08-10 18:29:17.087+00', NULL, 2, 46),
	(576, 'Tiên Phước', '2020-08-10 18:29:17.087+00', '2020-08-10 18:29:17.087+00', NULL, 2, 46),
	(581, 'Minh Long', '2020-08-10 18:29:17.088+00', '2020-08-10 18:29:17.088+00', NULL, 2, 47),
	(587, 'Sơn Tịnh', '2020-08-10 18:29:17.088+00', '2020-08-10 18:29:17.088+00', NULL, 2, 47),
	(592, 'Bình Liêu', '2020-08-10 18:29:17.089+00', '2020-08-10 18:29:17.089+00', NULL, 2, 48),
	(596, 'TX. Đông Triều', '2020-08-10 18:29:17.089+00', '2020-08-10 18:29:17.089+00', NULL, 2, 48),
	(600, 'TP. Móng Cái', '2020-08-10 18:29:17.089+00', '2020-08-10 18:29:17.089+00', NULL, 2, 48),
	(605, 'Cam Lộ', '2020-08-10 18:29:17.09+00', '2020-08-10 18:29:17.09+00', NULL, 2, 49),
	(610, 'Hải Lăng', '2020-08-10 18:29:17.09+00', '2020-08-10 18:29:17.09+00', NULL, 2, 49),
	(618, 'Long Phú', '2020-08-10 18:29:17.09+00', '2020-08-10 18:29:17.09+00', NULL, 2, 50),
	(621, 'TX. Ngã Năm', '2020-08-10 18:29:17.092+00', '2020-08-10 18:29:17.092+00', NULL, 2, 50),
	(630, 'Phù Yên', '2020-08-10 18:29:17.092+00', '2020-08-10 18:29:17.092+00', NULL, 2, 51),
	(626, 'Bắc Yên', '2020-08-10 18:29:17.092+00', '2020-08-10 18:29:17.092+00', NULL, 2, 51),
	(634, 'TP. Sơn La', '2020-08-10 18:29:17.092+00', '2020-08-10 18:29:17.092+00', NULL, 2, 51),
	(638, 'Bến Cầu', '2020-08-10 18:29:17.093+00', '2020-08-10 18:29:17.093+00', NULL, 2, 52),
	(643, 'Tân Biên', '2020-08-10 18:29:17.093+00', '2020-08-10 18:29:17.093+00', NULL, 2, 52),
	(648, 'Hưng Hà', '2020-08-10 18:29:17.093+00', '2020-08-10 18:29:17.093+00', NULL, 2, 53),
	(652, 'Thái Thụy', '2020-08-10 18:29:17.093+00', '2020-08-10 18:29:17.093+00', NULL, 2, 53),
	(658, 'TX. Phổ Yên', '2020-08-10 18:29:17.093+00', '2020-08-10 18:29:17.093+00', NULL, 2, 54);
INSERT INTO public.locations VALUES
	(662, 'TP. Thái Nguyên', '2020-08-10 18:29:17.094+00', '2020-08-10 18:29:17.094+00', NULL, 2, 54),
	(671, 'Lang Chánh', '2020-08-10 18:29:17.094+00', '2020-08-10 18:29:17.094+00', NULL, 2, 55),
	(667, 'Đông Sơn', '2020-08-10 18:29:17.094+00', '2020-08-10 18:29:17.094+00', NULL, 2, 55),
	(676, 'Như Xuân', '2020-08-10 18:29:17.094+00', '2020-08-10 18:29:17.094+00', NULL, 2, 55),
	(680, 'Quảng Xương', '2020-08-10 18:29:17.094+00', '2020-08-10 18:29:17.094+00', NULL, 2, 55),
	(684, 'Thiệu Hóa', '2020-08-10 18:29:17.094+00', '2020-08-10 18:29:17.094+00', NULL, 2, 55),
	(689, 'Vĩnh Lộc', '2020-08-10 18:29:17.095+00', '2020-08-10 18:29:17.095+00', NULL, 2, 55),
	(694, 'Nam Đông', '2020-08-10 18:29:17.095+00', '2020-08-10 18:29:17.095+00', NULL, 2, 56),
	(699, 'Quảng Điền', '2020-08-10 18:29:17.096+00', '2020-08-10 18:29:17.096+00', NULL, 2, 56),
	(704, 'Chợ Gạo', '2020-08-10 18:29:17.097+00', '2020-08-10 18:29:17.097+00', NULL, 2, 57),
	(707, 'Gò Công Tây', '2020-08-10 18:29:17.097+00', '2020-08-10 18:29:17.097+00', NULL, 2, 57),
	(712, 'Q. Bình Tân', '2020-08-10 18:29:17.097+00', '2020-08-10 18:29:17.097+00', NULL, 2, 58),
	(718, 'Nhà Bè', '2020-08-10 18:29:17.097+00', '2020-08-10 18:29:17.097+00', NULL, 2, 58),
	(724, 'Quận 5', '2020-08-10 18:29:17.097+00', '2020-08-10 18:29:17.097+00', NULL, 2, 58),
	(730, 'Quận 11', '2020-08-10 18:29:17.098+00', '2020-08-10 18:29:17.098+00', NULL, 2, 58),
	(737, 'Cầu Ngang', '2020-08-10 18:29:17.1+00', '2020-08-10 18:29:17.1+00', NULL, 2, 59),
	(740, 'Duyên Hải', '2020-08-10 18:29:17.1+00', '2020-08-10 18:29:17.1+00', NULL, 2, 59),
	(745, 'Hàm Yên', '2020-08-10 18:29:17.1+00', '2020-08-10 18:29:17.1+00', NULL, 2, 60),
	(749, 'TP. Tuyên Quang', '2020-08-10 18:29:17.1+00', '2020-08-10 18:29:17.1+00', NULL, 2, 60),
	(754, 'Mang Thít', '2020-08-10 18:29:17.101+00', '2020-08-10 18:29:17.101+00', NULL, 2, 61),
	(759, 'Bình Xuyên', '2020-08-10 18:29:17.101+00', '2020-08-10 18:29:17.101+00', NULL, 2, 62),
	(764, 'Tam Đảo', '2020-08-10 18:29:17.101+00', '2020-08-10 18:29:17.101+00', NULL, 2, 62),
	(768, 'Lục Yên', '2020-08-10 18:29:17.101+00', '2020-08-10 18:29:17.101+00', NULL, 2, 63),
	(772, 'Trấn Yên', '2020-08-10 18:29:17.102+00', '2020-08-10 18:29:17.102+00', NULL, 2, 63),
	(1, 'An Giang', NULL, '2020-08-19 09:59:08.438+00', NULL, 1, NULL),
	(100, 'An Giang', NULL, '2020-08-19 09:54:12.967+00', NULL, 2, 4);


--
-- Data for Name: rank; Type: TABLE DATA; Schema: public; Owner: root
--



--
-- Data for Name: rental_transaction; Type: TABLE DATA; Schema: public; Owner: root
--



--
-- Data for Name: roles; Type: TABLE DATA; Schema: public; Owner: root
--

INSERT INTO public.roles VALUES
	(1, 'admin', NULL, NULL, NULL),
	(2, 'reception', NULL, NULL, NULL);


--
-- Data for Name: rooms; Type: TABLE DATA; Schema: public; Owner: root
--



--
-- Name: customers_id_seq; Type: SEQUENCE SET; Schema: public; Owner: root
--

SELECT pg_catalog.setval('public.customers_id_seq', 83, true);


--
-- Name: employees_id_seq; Type: SEQUENCE SET; Schema: public; Owner: root
--

SELECT pg_catalog.setval('public.employees_id_seq', 8, true);


--
-- Name: employees_role_id_seq; Type: SEQUENCE SET; Schema: public; Owner: root
--

SELECT pg_catalog.setval('public.employees_role_id_seq', 1, false);


--
-- Name: province_id_seq; Type: SEQUENCE SET; Schema: public; Owner: root
--

SELECT pg_catalog.setval('public.province_id_seq', 776, true);


--
-- Name: rank_id_seq; Type: SEQUENCE SET; Schema: public; Owner: root
--

SELECT pg_catalog.setval('public.rank_id_seq', 1, false);


--
-- Name: rooms_id_seq; Type: SEQUENCE SET; Schema: public; Owner: root
--

SELECT pg_catalog.setval('public.rooms_id_seq', 1, false);


--
-- Name: employees employees_pkey; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.employees
    ADD CONSTRAINT employees_pkey PRIMARY KEY (id);


--
-- Name: roles employees_role_pkey; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.roles
    ADD CONSTRAINT employees_role_pkey PRIMARY KEY (id);


--
-- Name: locations province_pkey; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.locations
    ADD CONSTRAINT province_pkey PRIMARY KEY (id);


--
-- Name: customers unique_customers_code; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.customers
    ADD CONSTRAINT unique_customers_code UNIQUE (code);


--
-- Name: customers unique_customers_id; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.customers
    ADD CONSTRAINT unique_customers_id PRIMARY KEY (id);


--
-- Name: employees unique_employees_email; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.employees
    ADD CONSTRAINT unique_employees_email UNIQUE (email);


--
-- Name: roles unique_employees_role_id; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.roles
    ADD CONSTRAINT unique_employees_role_id UNIQUE (id);


--
-- Name: locations unique_province_id; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.locations
    ADD CONSTRAINT unique_province_id UNIQUE (id);


--
-- Name: rank unique_rank_id; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.rank
    ADD CONSTRAINT unique_rank_id PRIMARY KEY (id);


--
-- Name: rooms unique_rooms_code; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.rooms
    ADD CONSTRAINT unique_rooms_code UNIQUE (code);


--
-- Name: rooms unique_rooms_id; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.rooms
    ADD CONSTRAINT unique_rooms_id PRIMARY KEY (id);


--
-- Name: index_city_id; Type: INDEX; Schema: public; Owner: root
--

CREATE INDEX index_city_id ON public.customers USING btree (city_id);


--
-- Name: index_district_id; Type: INDEX; Schema: public; Owner: root
--

CREATE INDEX index_district_id ON public.customers USING btree (district_id);


--
-- Name: customers link_customer_city_location; Type: FK CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.customers
    ADD CONSTRAINT link_customer_city_location FOREIGN KEY (city_id) REFERENCES public.locations(id) MATCH FULL ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: customers link_customer_district_locations; Type: FK CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.customers
    ADD CONSTRAINT link_customer_district_locations FOREIGN KEY (district_id) REFERENCES public.locations(id) MATCH FULL ON UPDATE CASCADE ON DELETE SET NULL;


--
-- Name: rental_transaction link_customers_booking; Type: FK CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.rental_transaction
    ADD CONSTRAINT link_customers_booking FOREIGN KEY (customer_id) REFERENCES public.customers(id) MATCH FULL ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: employees link_employee_city_location; Type: FK CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.employees
    ADD CONSTRAINT link_employee_city_location FOREIGN KEY (city_id) REFERENCES public.locations(id) MATCH FULL ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: employees link_employee_district_location; Type: FK CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.employees
    ADD CONSTRAINT link_employee_district_location FOREIGN KEY (district_id) REFERENCES public.locations(id) MATCH FULL ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: rental_transaction link_employee_rental; Type: FK CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.rental_transaction
    ADD CONSTRAINT link_employee_rental FOREIGN KEY (employee_id) REFERENCES public.employees(id) MATCH FULL ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: locations link_locations_locations; Type: FK CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.locations
    ADD CONSTRAINT link_locations_locations FOREIGN KEY (parent_id) REFERENCES public.locations(id) MATCH FULL ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: employees link_role_employee; Type: FK CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.employees
    ADD CONSTRAINT link_role_employee FOREIGN KEY (role_id) REFERENCES public.roles(id) MATCH FULL ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: rental_transaction link_room_rental; Type: FK CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.rental_transaction
    ADD CONSTRAINT link_room_rental FOREIGN KEY (room_id) REFERENCES public.rooms(id) MATCH FULL ON UPDATE CASCADE ON DELETE CASCADE;


--
-- PostgreSQL database dump complete
--

