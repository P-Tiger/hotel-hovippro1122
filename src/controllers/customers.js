
import {
    Customers,
    Locations
} from '../models/index'

import { Op } from 'sequelize'
import moment from 'moment';


async function getOne(id) {
    let data = await Customers.findOne({
        where: {
            id: id
        },
        include: [
            {
                model: Locations,
                as: 'city_info',
                attributes: ['id', 'name', 'type', 'parent_id']
            },
            {
                model: Locations,
                as: 'district_info',
                attributes: ['id', 'name', 'type', 'parent_id']
            },
        ]
    })
    return data;
}


async function createCustomer(req, res) {
    let {
        code,
        name,
        city_id,
        district_id,
        address,
        gender,
        birth_date,
        phone_number
    } = req.body

    if (!code) {
        return res.status(400).send("code must required")
    }

    if (!name) {
        return res.status(400).send("name must required")
    }

    if (!city_id) {
        return res.status(400).send("city_id must required")
    }

    if (!district_id) {
        return res.status(400).send("district_id must required")
    }

    if (!address) {
        return res.status(400).send("address must required")
    }

    if (!gender) {
        return res.status(400).send("gender must required")
    }

    if (!birth_date) {
        return res.status(400).send("birth_date must required")
    }

    if (!phone_number) {
        return res.status(400).send("phone_number must required")
    }


    let data = {
        code: code,
        name: name,
        city_id: city_id,
        district_id: district_id,
        address: address,
        gender: gender,
        birth_date: birth_date,
        phone_number: phone_number,
    }

    let newCustomer = null

    try {
        newCustomer = await Customers.create(data)
    } catch (error) {
        return res.status(500).send(error);
    }

    let dataGetOne = await getOne(newCustomer.id)

    return res.status(200).send(dataGetOne);
}


async function findAllCustomer(req, res) {
    let {
        page,
        perPage,
        code,
        name,
        from_date,
        to_date,
        // birth_date,
        city_id
    } = req.query

    let pages = page || 1;
    let perPages = perPage || 10

    let where = {};

    if (code) {
        where.code = {
            [Op.substring]: `${code}`
        };
    }

    if (name) {
        where.name = {
            [Op.substring]: `${name}`
        };
    }


    if (from_date && to_date) {
        where.createdAt = {
            [Op.between]: [moment(from_date).format('YYYY-MM-DD'), moment(to_date).format('YYYY-MM-DD')]
        }
    }

    if (city_id) {
        where.city_id = city_id
    }


    let data = await Customers.findAll({
        limit: perPages,
        offset: (pages - 1) * perPages,
        order: [
            ['id', 'ASC'],
        ],
        where: where,
        include: [
            {
                model: Locations,
                as: 'city_info',
                attributes: ['id', 'name', 'type', 'parent_id']
            },
            {
                model: Locations,
                as: 'district_info',
                attributes: ['id', 'name', 'type', 'parent_id']
            },
        ]
    })


    return res.status(200).send({
        total: Object.keys(data).length,
        data: data
    });
}



async function findOneCustomer(req, res) {
    let {
        id
    } = req.params
    let dataGetOne = await getOne(id)

    return res.status(200).send(dataGetOne);
}


async function updateCustomer(req, res) {
    let {
        id
    } = req.params

    let dataGetOne = await getOne(id);

    if (!dataGetOne) {
        return res.status(404).send({ Message: 'Customer not found' })
    }

    let {
        code,
        name,
        city_id,
        address,
        gender,
        birth_date,
        phone_number
    } = req.body

    let data_update = {};

    if (code) {
        data_update.code = code
    }

    if (name) {
        data_update.name = name
    }

    if (city_id) {
        data_update.city_id = city_id
    }

    if (address) {
        data_update.address = address
    }

    if (gender) {
        data_update.gender = gender
    }

    if (birth_date) {
        data_update.birth_date = birth_date
    }

    if (phone_number) {
        data_update.phone_number = phone_number
    }

    try {
        await Customers.update(data_update, { where: { id: id } })
    } catch (error) {
        return res.status(400).send(error.message);
    }

    return res.status(200).send(await getOne(id));
}


export {
    createCustomer,
    findAllCustomer,
    findOneCustomer,
    updateCustomer
}