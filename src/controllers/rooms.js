import {
    Rooms,
    Ranks
} from '../models/index'

async function getOne(id) {
    let data = await Rooms.findOne({
        where: {
            id: id
        },
        include: [
            {
                model: Ranks,
                as: 'rank_info',
                attributes: ['id', 'name', 'type', 'parent_id']
            },
            {
                model: Ranks,
                as: 'rank_type_info',
                attributes: ['id', 'name', 'type', 'parent_id']
            },
        ]
    })
    return data;
}



async function createRoom(req, res) {
    let {
        floor,
        name,
        status,
        rank_id,
        rank_type_id,
        price_rental,
        start_time,
        end_time
    } = req.body

    if (!name) {
        return res.status(400).send("code must required")
    }

    if (!floor) {
        return res.status(400).send("floor must required")
    }

    if (!status) {
        return res.status(400).send("status must required")
    }

    if (!rank_id) {
        return res.status(400).send("rank_id must required")
    }

    if (!rank_type_id) {
        return res.status(400).send("rank_type_id must required")
    }

    if (!price_rental) {
        return res.status(400).send("price_rental must required")
    }

    let data = {
        name: name,
        floor: floor,
        status: status,
        start_time: start_time ? start_time : null,
        end_time: end_time ? end_time : null,
        rank_id: rank_id,
        rank_type_id: rank_type_id,
        price_rental: price_rental,
    }


    let newRooms = null;

    try {
        newRooms = await Rooms.create(data)
    } catch (error) {
        return res.status(500).send(error);
    }

    let dataGetOne = await getOne(newRooms.id)

    return res.status(200).send(dataGetOne);
}

async function findAllRoom(req, res) {
    let {
        name,
        status,
        rank_id,
        rank_type_id,
        price_rental_from,
        price_rental_to,
        start_time,
        end_time,
        page,
        perPage
    } = req.body



    let pages = page || 1;
    let perPages = perPage || 10

    let where = {};

    if (name) {
        where.name = {
            [Op.substring]: `${name}`
        };
    }

    if (status) {
        where.status = status
    }

    if (rank_id) {
        where.rank_id = rank_id
    }

    if (rank_type_id) {
        where.rank_type_id = rank_type_id
    }

    if (price_rental_from && price_rental_to) {
        where.price_rental = {
            [Op.between]: [price_rental_from, price_rental_to]
        }
    }

    if (from_date && to_date) {
        where.createdAt = {
            [Op.between]: [moment(from_date).format('YYYY-MM-DD'), moment(to_date).format('YYYY-MM-DD')]
        }
    }

    // let data = {
    //     code: code,
    //     floor: floor,
    //     status: status,
    //     start_time: start_time ? start_time : null,
    //     end_time: end_time ? end_time : null,
    //     rank_id: rank_id,
    //     rank_type_id: rank_type_id,
    //     price_rental: price_rental,
    // }


    // let newRooms = null;

    // try {
    //     newRooms = await Rooms.create(data)
    // } catch (error) {
    //     return res.status(500).send(error);
    // }

    // let dataGetOne = await getOne(newRooms.id)

    // return res.status(200).send(dataGetOne);
}



export {
    createRoom,
    findAllRoom,
}