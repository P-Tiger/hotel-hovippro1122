import {
    Ranks
} from '../models/index'


async function getOne(id) {
    let data = await Ranks.findOne({
        where: {
            id: id
        },
    })
    return data;
}


async function createRank(req, res) {
    let {
        name,
        parent_id,
        type,
    } = req.body

    if (!name) {
        return res.status(400).send("Name must required")
    }

    if (!type) {
        return res.status(400).send("Type must required")
    }

    let data = {
        name: name,
        parent_id: parent_id === 0 ? null : parent_id,
        type: type
    }

    let newRank = null;

    try {
        newRank = await Ranks.create(data);
    } catch (error) {
        res.status(500).send(error);
    }


    return res.status(200).send(await getOne(newRank.id))
}


async function findAllRank(req, res) {
    let {
        page,
        perPage,
        parent_id,
        type
    } = req.query

    let pages = page || 1;
    let perPages = perPage || 10

    let where = {};

    if (type) {
        where.type = type;
    }

    if (parent_id) {
        where.parent_id = parent_id;
    }


    let data = await Ranks.findAll({
        limit: perPages,
        offset: (pages - 1) * perPages,
        where: where,
        order: [
            ['id', 'ASC'],
        ],
    })

    return res.status(200).send({
        total: Object.keys(data).length,
        data: data
    });
}



async function findOneRank(req, res) {
    let {
        id
    } = req.params

    let dataGetOne = await getOne(id)

    return res.status(200).send(dataGetOne);
}


async function updateRank(req, res) {
    let {
        id
    } = req.params

    let dataGetOne = await getOne(id);

    if (!dataGetOne) {
        return res.status(404).send({ Message: 'Location not found' })
    }

    let {
        name,
        type,
        parent_id,
    } = req.body

    let data_update = {};

    if (name) {
        data_update.name = name
    }

    if (type) {
        data_update.type = type
    }

    if (parent_id) {
        data_update.parent_id = parent_id
    }

    if (parent_id === 0) {
        data_update.parent_id = null
    }

    try {
        await Ranks.update(data_update, { where: { id: id } })
    } catch (error) {
        return res.status(400).send(error.message);
    }

    return res.status(200).send(await getOne(id));
}


export {
    createRank,
    findAllRank,
    findOneRank,
    updateRank
}