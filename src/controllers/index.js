const locations = require('./locations');
const customers = require('./customers');
const employees = require('./employees');
const auth = require('./auth');
const rooms = require('./rooms')
const ranks = require('./ranks')



module.exports = {
    locations,
    customers,
    employees,
    auth,
    rooms,
    ranks
}