import {
    Employees
} from '../models/index'

import jwt from 'jsonwebtoken'
import bcrypt from 'bcryptjs'


async function login(req, res) {
    let {
        email,
        password
    } = req.body;

    if (email && password) {
        let employee = await Employees.findOne({
            where: {
                email: email
            }
        });

        if (!employee) {
            return res.status(401).json({
                "message": "Incorrect email or password",
            });
        }

        bcrypt.compare(password, employee.password).then(isMatch => {
            if (isMatch) {
                let payload = {
                    id: employee.id,
                    email: employee.email,
                    role: employee.role
                }

                let token = jwt.sign(payload, process.env.SECRETORKEY, {
                    expiresIn: '30d' // 1 month
                })

                return res.json({
                    token: token,
                    id: employee.id,
                    email: employee.email,
                    role: employee.role_id
                });
            }
        })
    } else {
        return res.status(401).json({
            "message": "Incorrect email or password",
        });
    }
}

export {
    login
}