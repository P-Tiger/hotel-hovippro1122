

import {
    Employees,
    Locations,
    Roles
} from '../models/index'

import bcrypt from 'bcryptjs'



async function getOne(id) {
    let data = await Employees.findOne({
        where: {
            id: id
        },
        include: [
            {
                model: Locations,
                as: 'cities_info',
                attributes: ['id', 'name', 'type', 'parent_id']
            },
            {
                model: Locations,
                as: 'districts_info',
                attributes: ['id', 'name', 'type', 'parent_id']
            },
            {
                model: Roles,
                as: 'role_info',
                attributes: ['id', 'name']
            },
        ]
    })
    return data;
}



async function createEmployee(req, res) {
    let {
        email,
        password,
        name,
        city_id,
        district_id,
        address,
        gender,
        birth_date,
        phone_number,
        role_id
    } = req.body

    // check require
    if (!email) {
        return res.status(400).send({ email: "email is require" })
    }
    if (!password) {
        return res.status(400).send({ password: "password is require" })
    }
    if (!name) {
        return res.status(400).send({ name: "name is require" })
    }
    if (!city_id) {
        return res.status(400).send({ city: "city is require" })
    }
    if (!district_id) {
        return res.status(400).send({ district: "district is require" })
    }
    if (!address) {
        return res.status(400).send({ address: "address is require" })
    }
    if (!gender) {
        return res.status(400).send({ gender: "gender is require" })
    }
    if (!birth_date) {
        return res.status(400).send({ birth_date: "birth_date is require" })
    }
    if (!phone_number) {
        return res.status(400).send({ phone_number: "phone_number is require" })
    }
    if (!role_id) {
        return res.status(400).send({ role: "role_id is require" })
    }

    let checkExistEmployee = await Employees.findOne({ where: { email: req.body.email } });

    if (checkExistEmployee) {
        return res.status(400).send({ email: "Email already exists" })
    };

    let data = {
        email: email,
        password: password,
        name: name,
        city_id: city_id,
        district_id: district_id,
        address: address,
        gender: gender,
        birth_date: birth_date,
        phone_number: phone_number,
        role_id: role_id
    }

    let newEmployee = null;

    try {
        newEmployee = await Employees.create(data);
    } catch (err) {
        return res.status(400).send(err);
    }

    bcrypt.genSalt(10, (err, salt) => {
        bcrypt.hash(newEmployee.password, salt, async (err, hash) => {
            if (err) throw err;
            newEmployee.password = hash;
            await newEmployee.save();
        })
    })

    let dataGetOne = await getOne(newEmployee.id);

    return res.status(200).send(dataGetOne);
}


async function findAllEmployee(req, res) {
    let {
        name,
        city_id,
        from_date,
        to_date,
        page,
        perPage
    } = req.body

    let pages = page || 1;
    let perPages = perPage || 10

    let where = {};

    if (name) {
        where.name = {
            [Op.substring]: `${name}`
        };
    }

    if (city_id) {
        where.city_id = city_id
    }

    if (from_date && to_date) {
        where.createdAt = {
            [Op.between]: [moment(from_date).format('YYYY-MM-DD'), moment(to_date).format('YYYY-MM-DD')]
        }
    }

    let data = await Employees.findAll({
        limit: perPages,
        offset: (pages - 1) * perPages,
        order: [
            ['id', 'ASC'],
        ],
        where: where,
        include: [
            {
                model: Locations,
                as: 'cities_info',
                attributes: ['id', 'name', 'type', 'parent_id']
            },
            {
                model: Locations,
                as: 'districts_info',
                attributes: ['id', 'name', 'type', 'parent_id']
            },
            {
                model: Roles,
                as: 'role_info'
            },
        ]
    })

    return res.status(200).send({
        total: Object.keys(data).length,
        data: data
    });
}


async function findOneEmployee(req, res) {
    let {
        id
    } = req.params

    let dataGetOne = await getOne(id);

    return res.status(200).send(dataGetOne)
}


async function updateEmployee(req, res) {
    let {
        id
    } = req.params

    let dataGetOne = await getOne(id);

    if (!dataGetOne) {
        return res.status(404).send({ Message: 'Employee not found' })
    }

    let {
        name,
        city_id,
        district_id,
        address,
        gender,
        birth_date,
        phone_number,
        role_id
    } = req.body

    let data_update = {};

    if (name) {
        data_update.name = name
    }

    if (city_id) {
        data_update.city_id = city_id
    }

    if (district_id) {
        data_update.district_id = district_id
    }

    if (address) {
        data_update.address = address
    }

    if (gender) {
        data_update.gender = gender
    }

    if (birth_date) {
        data_update.birth_date = birth_date
    }

    if (phone_number) {
        data_update.phone_number = phone_number
    }

    if (role_id) {
        data_update.role_id = role_id
    }

    try {
        await Employees.update(data_update, { where: { id: id } })
    } catch (error) {
        return res.status(400).send(error.message);
    }

    return res.status(200).send(await getOne(id));
}



export {
    createEmployee,
    findAllEmployee,
    findOneEmployee,
    updateEmployee
}