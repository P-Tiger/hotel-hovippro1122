
const {
    auth,
    customers,
    employees,
    locations,
    rooms,
    ranks
} = require('../controllers/index');

const verifyToken = require('../middlewares/verifyToken').verifyToken;

module.exports = (app) => {
    app.post('/login', auth.login);

    app.get('/locations', verifyToken, locations.findAllLocation)
    app.get('/locations/:id', verifyToken, locations.findOneLocation)
    app.put('/locations/:id', verifyToken, locations.updateLocation)

    app.post('/customers', verifyToken, customers.createCustomer)
    app.get('/customers', verifyToken, customers.findAllCustomer)
    app.get('/customers/:id', verifyToken, customers.findOneCustomer)
    app.put('/customers/:id', verifyToken, customers.updateCustomer)

    app.post('/employees', verifyToken, employees.createEmployee)
    app.get('/employees', verifyToken, employees.findAllEmployee)
    app.get('/employees/:id', verifyToken, employees.findOneEmployee)
    app.put('/employees/:id', verifyToken, employees.updateEmployee)

    app.post('/rooms', verifyToken, rooms.createRoom)

    // Standard
    app.post('/ranks', verifyToken, ranks.createRank)
    app.get('/ranks', verifyToken, ranks.findAllRank)
    app.get('/ranks/:id', verifyToken, ranks.findOneRank)
    app.post('/ranks/:id', verifyToken, ranks.updateRank)
}