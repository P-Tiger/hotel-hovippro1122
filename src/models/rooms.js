
module.exports = (sequelize, DataTypes) => {
    const rooms = sequelize.define('rooms', {
        name: {
            type: DataTypes.STRING,
            allowNull: false,
            unique: true
        },
        status: {
            type: DataTypes.INTEGER,
            allowNull: false
        },
        floor: {
            type: DataTypes.INTEGER,
            allowNull: false
        },
        start_time: {
            type: DataTypes.DATE,
            allowNull: true
        },
        end_time: {
            type: DataTypes.DATE,
            allowNull: true
        },
        rank_id: {
            type: DataTypes.INTEGER,
            allowNull: false
        },
        rank_type_id: {
            type: DataTypes.INTEGER,
            allowNull: false
        },
        price_rental: {
            type: DataTypes.DOUBLE,
            allowNull: false
        },
        extra: {
            type: DataTypes.JSONB,
            allowNull: true
        },
        meta: {
            type: DataTypes.JSONB,
            allowNull: true
        },
        createdAt: {
            allowNull: true,
            type: DataTypes.DATE
        },
        updatedAt: {
            allowNull: true,
            type: DataTypes.DATE
        },
        deletedAt: {
            allowNull: true,
            type: DataTypes.DATE
        },
    }, { timestamps: true });
    return rooms;
}