import Sequelize from 'sequelize'

const sequelize = new Sequelize(process.env.DB_DATABASE, process.env.DB_USERNAME, process.env.DB_PASSWORD, {
    host: process.env.DB_HOST,
    port: process.env.DB_PORT,
    dialect: process.env.DB_DIALECT
});

// check connecting database
try {
    sequelize.authenticate();
    console.log('Connection has been established successfully.');
} catch (error) {
    console.error('Unable to connect to the database:', error);
}


const db = {};

db.Sequelize = Sequelize;
db.sequelize = sequelize;

db.locations = require("./locations.js")(sequelize, Sequelize);
db.customers = require("./customers.js")(sequelize, Sequelize);
db.employees = require("./employees.js")(sequelize, Sequelize);
db.roles = require("./roles.js")(sequelize, Sequelize);
db.rooms = require("./rooms.js")(sequelize, Sequelize);
db.ranks = require("./ranks.js")(sequelize, Sequelize);




db.locations.hasMany(db.locations, {
    sourceKey: 'id',
    foreignKey: 'parent_id',
})

db.locations.belongsTo(db.locations, {
    targetKey: 'id',
    foreignKey: 'parent_id',
})


db.locations.hasMany(db.customers, {
    sourceKey: 'id',
    foreignKey: 'city_id',
    as: 'city_info',
})

db.customers.belongsTo(db.locations, {
    targetKey: 'id',
    foreignKey: 'city_id',
    as: 'city_info'
})


db.locations.hasMany(db.customers, {
    sourceKey: 'id',
    foreignKey: 'district_id',
    as: 'district_info'
})

db.customers.belongsTo(db.locations, {
    targetKey: 'id',
    foreignKey: 'district_id',
    as: 'district_info'
})

db.locations.hasMany(db.employees, {
    sourceKey: 'id',
    foreignKey: 'city_id',
    as: 'cities_info'
})

db.employees.belongsTo(db.locations, {
    targetKey: 'id',
    foreignKey: 'city_id',
    as: 'cities_info',
})

db.locations.hasMany(db.employees, {
    sourceKey: 'id',
    foreignKey: 'district_id',
    as: 'districts_info',
})

db.employees.belongsTo(db.locations, {
    targetKey: 'id',
    foreignKey: 'district_id',
    as: 'districts_info',
})

db.roles.hasMany(db.employees, {
    sourceKey: 'id',
    foreignKey: 'role_id',
    as: 'role_info',
})

db.employees.belongsTo(db.roles, {
    targetKey: 'id',
    foreignKey: 'role_id',
    as: 'role_info',
})

db.ranks.hasMany(db.ranks, {
    sourceKey: 'id',
    foreignKey: 'parent_id',
})

db.ranks.belongsTo(db.ranks, {
    targetKey: 'id',
    foreignKey: 'parent_id',
})

db.ranks.hasMany(db.rooms, {
    sourceKey: 'id',
    foreignKey: 'rank_id',
    as: 'rank_info',
})

db.rooms.belongsTo(db.ranks, {
    targetKey: 'id',
    foreignKey: 'rank_id',
    as: 'rank_info'
})


db.ranks.hasMany(db.rooms, {
    sourceKey: 'id',
    foreignKey: 'rank_type_id',
    as: 'rank_type_info'
})

db.rooms.belongsTo(db.ranks, {
    targetKey: 'id',
    foreignKey: 'rank_type_id',
    as: 'rank_type_info'
})



module.exports = {
    Locations: db.locations,
    Employees: db.employees,
    Roles: db.roles,
    Customers: db.customers,
    Rooms: db.rooms,
    Ranks: db.ranks
};
