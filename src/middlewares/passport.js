const JwtStrategy = require("passport-jwt").Strategy;
const ExtractJwt = require("passport-jwt").ExtractJwt;
const models = require('../models/index');

const opts = {};

opts.jwtFromRequest = ExtractJwt.fromAuthHeaderAsBearerToken();
opts.secretOrKey = process.env.SECRETORKEY;

// middleware auth
module.exports = passport => {
    const strategy = new JwtStrategy(opts, (payload, next) => {
        console.log('payload received', payload);
        const customer = models.customers.findOne({ where: { id: payload.id } });
        if (customer) next(null, customer);
        else next(null, false)
    })

    passport.use(strategy);
}