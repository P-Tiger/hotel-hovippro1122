
import jwt from 'jsonwebtoken'

module.exports = {
    verifyToken(req, res, next) {
        const bearerHeader = req.headers['authorization'];
        try {
            const token = bearerHeader.split(' ');
            const decode = jwt.verify(token[1], process.env.SECRETORKEY);
            if (decode) {
                next()
            };
        } catch (error) {
            return res.status(401).send({
                error: error,
                format: `Bad Authorization header format. Format is "Authorization: Bearer <token>"`
            });
        }
    }
}