const express = require('express');
const logger = require('morgan');
const bodyParser = require('body-parser');
const passport = require('passport');
const winston = require('./src/configs/winston');
const app = express();

require('dotenv').config();

app.enable('trust proxy')

logger.token('ip', function (req, res) {
    const ipAddress = req.ip.split(':')
    return ipAddress[ipAddress.length - 1] === '1' ? req.hostname : ipAddress[ipAddress.length - 1]
});

app.use(logger(':ip :remote-user :method :url :status - :response-time ms', { stream: winston.stream }))

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.use(passport.initialize());


require("./src/middlewares/passport")(passport);
require('./src/routers')(app)


app.get('/', (req, res) => res.status(200).send({
    message: 'Welcome to the beginning of nothingness.',
}));

module.exports = app;